﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MemoQ_Provider_Service")]
[assembly: AssemblyDescription("Lionbridge MemoQ provider connector")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Lionbridge")]
[assembly: AssemblyProduct("MemoQ_Connector_Service")]
[assembly: AssemblyCopyright("Copyright © Lionbridge 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("84a54c6e-70be-46c6-b50b-28de8b2da693")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.3.0.0")]
[assembly: AssemblyFileVersion("1.3.0.0")]
