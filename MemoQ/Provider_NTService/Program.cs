using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace MemoQ_Provider_Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;

            // More than one user Service may run within the same process. To add
            // another service to this process, change the following line to
            // create a second service object. For example,
            //
            //   ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
            //
            ServicesToRun = new ServiceBase[] { new Service1() };

            ServiceBase.Run(ServicesToRun);
        }

        static public string CONFIG_SERVICE_NAME = "ClayTablet.Provider.ServiceName";
        static public string getServiceName() {
            if (System.Configuration.ConfigurationManager.AppSettings[CONFIG_SERVICE_NAME] != null)
            {
                return System.Configuration.ConfigurationManager.AppSettings[CONFIG_SERVICE_NAME].ToString();
            } else
            {
                return "";
            }
        }
    }
}