using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.IO;
using ClayTablet.CT.Utility;
using System.Reflection;

namespace MemoQ_Provider_Service
{
    public partial class Service1 : ServiceBase
    {
        protected System.Timers.Timer timer;
        protected bool IsRunning = false;
        private string assembly_version;
        private string assembly_name;
        private int pollingTMSTimeOut = 0;

        public Service1()
        {
            String path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            path = System.IO.Path.GetDirectoryName(path);
            Directory.SetCurrentDirectory(path);

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            assembly_version = fvi.ProductVersion;
            assembly_name = fvi.ProductName;

            InitializeComponent();

            this.ServiceName = Program.getServiceName();
        }

        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry(this.ServiceName + "[" + assembly_name + "-" + assembly_version + "] started.");
            CLogger.WriteLog(ELogLevel.INFO, this.ServiceName + "[" + assembly_name + "-" + assembly_version + "] started.");
            InitializeTimer();
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            timer.Enabled = false;
            EventLog.WriteEntry(this.ServiceName + "[" + assembly_name + "-" + assembly_version + "] stopped.");
            CLogger.WriteLog(ELogLevel.INFO, this.ServiceName + "[" + assembly_name + "-" + assembly_version + "] stopped.");
        }

        protected void InitializeTimer()
        {
            if (timer == null)
            {
                timer = new System.Timers.Timer();
                timer.AutoReset = true;
                timer.Interval = 1000;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            }
        }

        private void timer_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            if (!IsRunning)
            {
                try
                {
                    IsRunning = true;
                    timer.Enabled = false;

                    CLogger.WriteLog(ELogLevel.INFO,
                            "Polling by [" + assembly_name + "-" + assembly_version + "] at " + Directory.GetCurrentDirectory());

                    ClayTablet.MemoQ.ReceiverJob receiverJober = new ClayTablet.MemoQ.ReceiverJob();
                    receiverJober.Process();
                    pollingTMSTimeOut -= (int)timer.Interval;
                    if (_SendTransaltion())
                    {
                        receiverJober.SendNotifications();
                    }

                    timer.Interval = ReadAppSettingInterval();
                    CLogger.WriteLog(ELogLevel.INFO, "Polling done, next polling in " + timer.Interval / 1000 + " seconds");
                    timer.AutoReset = true;
                    IsRunning = false;
                    timer.Enabled = true;

                }
                catch (Exception exp)
                {
                    EventLog.WriteEntry(exp.Message);
                    CLogger.WriteLog(ELogLevel.ERROR, "Error: " + exp.Message, exp);
                }
                finally
                {

                    timer.Enabled = true;
                    IsRunning = false;
                }


            }
        }

        private bool _SendTransaltion()
        {
            if (pollingTMSTimeOut <= 0)
            {
                pollingTMSTimeOut = ReadMemoQPollingInterval();
                ClayTablet.MemoQ.ProviderSenderJob senderJober = new ClayTablet.MemoQ.ProviderSenderJob();
                senderJober.Process();
                return true;
            }
            else
            {
                CLogger.WriteLog(ELogLevel.DEBUG, "Next polling of MemoQ server in " + pollingTMSTimeOut / 1000 + " seconds");
                return false;
            }
        }


        protected int ReadAppSettingInterval()
        {
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SleepTime"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SleepTime"], 10);
                }
            }
            catch (Exception exp)
            {
                CLogger.WriteLog(ELogLevel.WARN, "Cannot read configured sleep time, use 30 seconds as default", exp);
            }

            return 30000;
        }

        protected int ReadMemoQPollingInterval()
        {
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MemoQ_Polling_Interval"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MemoQ_Polling_Interval"], 10);
                }
            }
            catch (Exception exp)
            {
                CLogger.WriteLog(ELogLevel.WARN, "Cannot read configured MemoQ polling interval, use 360 seconds as default", exp);
            }

            return 360000;
        }
    }
}
