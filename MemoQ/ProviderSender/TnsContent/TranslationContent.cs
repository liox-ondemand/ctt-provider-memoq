using System;
using System.Collections.Generic;
using System.Text;
using Xstream.Core;
using System.Xml;
using ClayTablet.CT.Utility; 

namespace ClayTablet.SC.Translation
{
    /* This class will be used to gether all the sitecore items with fields for translation, 
     * CT2 connector then will save it to a xml file and send out to CTT platform as one asset.
     *  
     *
     */
    [Serializable]
    public class TranslationContent
    {
        private String m_ct2ProjectId;   //CT2 Project ID
        private String m_ct2AssetId;   //CT2 Asset ID 
        private String m_sitecoreSourceLanguageCode;   //SC Source Language ID 
        private String m_ct2SourceLanguageCode;   //CT2 Language Code
        private String m_sitecoreTargetLanguageCode;   //SC Target Language Code 
        private String m_ct2TargetLanguageCode;   //CT2 Target Language Code
        private List<SitecoreItem> m_sitecoreItems = new List<SitecoreItem>();

        public String CT2ProjectId
        {
            get { return this.m_ct2ProjectId; }
            set { this.m_ct2ProjectId = value; }
        }

        public String CT2AssetId
         {
             get { return this.m_ct2AssetId; }
             set { this.m_ct2AssetId = value; }
         }

         public String SitecoreSourceLanguageCode
         {
             get { return this.m_sitecoreSourceLanguageCode; }
             set { this.m_sitecoreSourceLanguageCode = value; }
         }

         public String CT2SourceLanguageCode
         {
             get { return this.m_ct2SourceLanguageCode; }
             set { this.m_ct2SourceLanguageCode = value; }
         }

        public String SitecoreTargetLanguageCode
        {
            get { return this.m_sitecoreTargetLanguageCode; }
            set { this.m_sitecoreTargetLanguageCode = value; }
        }

        public String CT2TargetLanguageCode
        {
            get { return this.m_ct2TargetLanguageCode; }
            set { this.m_ct2TargetLanguageCode = value; }
        }

        public void AddSitecoreItem(SitecoreItem   sitecoreItem)
        {
            m_sitecoreItems.Add(sitecoreItem);
        }

        public int SitecoreItemsCount
        {
            get { return this.m_sitecoreItems.Count; } 
        }

        public int SitecoreItemFieldsCount
        {
            get {

                int totalFCount = 0;
                foreach (SitecoreItem sitecoreItem in m_sitecoreItems)
                {
                    totalFCount = totalFCount + sitecoreItem.ItemFieldsCount;
                }

                return totalFCount; 
            
            }
        }

        public List<SitecoreItem> SitecoreItems
        {
            get { return this.m_sitecoreItems; }
            set { this.m_sitecoreItems = value; }
        }

        public static TranslationContent readTranslationContentFromXMLFile(String xmlfileNamePath)
        {
            if (!System.IO.File.Exists(xmlfileNamePath))
            {
                CLogger.WriteLog(ELogLevel.ERROR, "readTranslationContentFromXMLFile Error: passed file doesn't exist.");
                CLogger.WriteLog(ELogLevel.ERROR, "Passed File: " + xmlfileNamePath);
                return null;

            }

            TranslationContent translationContent = new TranslationContent();

            XmlDocument xmldoc = new XmlDocument();
            try
            {
               // xmldoc.LoadXml(com.claytablet.util.FileUtil.ReadStringFromFile(xmlfileNamePath));
                xmldoc.Load(xmlfileNamePath);
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "readTranslationContentFromXMLFile Error: " + e.Message);
                return null;
            }

            XmlNodeList xmlContentNodeList = xmldoc.GetElementsByTagName("TranslationContent");
            if (xmlContentNodeList.Count != 1)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "readTranslationContentFromXMLFile Error: xmlContentNodeList.Count != 1");
                return null;
            }

            XmlNode xmlContentNode = xmlContentNodeList[0];
            XmlAttributeCollection xmlattrc = xmlContentNode.Attributes;
            foreach (XmlAttribute xmlAttribute in xmlattrc)
            {
                if (String.Compare(xmlAttribute.Name, "CT2ProjectId", true) == 0)
                    translationContent.CT2ProjectId = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "CT2AssetId", true) == 0)
                       translationContent.CT2AssetId = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "CT2SourceLanguageCode", true) == 0)
                       translationContent.CT2SourceLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "CT2TargetLanguageCode", true) == 0)
                       translationContent.CT2TargetLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "SitecoreSourceLanguageCode", true) == 0)
                       translationContent.SitecoreSourceLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "SitecoreTargetLanguageCode", true) == 0)
                       translationContent.SitecoreTargetLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "CT2SourceLangaugeCode", true) == 0)
                    translationContent.CT2SourceLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "CT2TargetLangaugeCode", true) == 0)
                    translationContent.CT2TargetLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "SitecoreSourceLangaugeCode", true) == 0)
                    translationContent.SitecoreSourceLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "SitecoreTargetLangaugeCode", true) == 0)
                    translationContent.SitecoreTargetLanguageCode = xmlAttribute.Value; 
            }

            XmlNodeList xmlSiteItemNodeList = xmldoc.GetElementsByTagName("SitecoreItem");
            foreach (XmlNode xmlNode in xmlSiteItemNodeList)
            {
                SitecoreItem newSitecoreItem = new SitecoreItem();

                #region get ID etc info
                XmlAttributeCollection xmlattrc_Item = xmlNode.Attributes;
                foreach (XmlAttribute xmlAttribute in xmlattrc_Item)
                {
                    if (String.Compare(xmlAttribute.Name, "DatabaseName", true) == 0)
                        newSitecoreItem.DatabaseName = xmlAttribute.Value;
                    else
                        if (String.Compare(xmlAttribute.Name, "ItemId", true) == 0)
                            newSitecoreItem.ItemId = xmlAttribute.Value;
                        else
                            if (String.Compare(xmlAttribute.Name, "ItemTargetVersion", true) == 0)
                                newSitecoreItem.ItemTargetVersion = int.Parse(xmlAttribute.Value);
                            else
                                if (String.Compare(xmlAttribute.Name, "ItemSourceVersion", true) == 0)
                                    newSitecoreItem.ItemSourceVersion = int.Parse(xmlAttribute.Value);
                }
                #endregion get ID etc info

                #region get Item Fields
                XmlNodeList fieldsNoteList = xmlNode.ChildNodes;
                foreach (XmlNode fieldNode in fieldsNoteList)
                {
                    if (String.Compare(fieldNode.Name, "FieldContent", true) == 0)
                    {

                        foreach( XmlAttribute xmlAttribute in fieldNode.Attributes)
                        {
                            if (String.Compare(xmlAttribute.Name, "FieldName", true) == 0)
                            {
                                
                                ItemField newItemField = new ItemField();
                                newItemField.FieldName = fieldNode.Attributes[0].Value;

                                newItemField.FieldContent = fieldNode.InnerText;

                                newSitecoreItem.AddItemField(newItemField);
                                break;
                            }
                        }
                    }
                }

                #endregion get Item Fields

                translationContent.AddSitecoreItem(newSitecoreItem);
            }

            return translationContent;
        }
      
        public static void writeTranslationContentToXMLFile(TranslationContent translationContent, String xmlfileNamePath)
        {
             

            //XmlTextWriter xmlWriter = new XmlTextWriter(xmlfileNamePath, System.Text.Encoding.UTF8);
           // xmlWriter.Formatting = Formatting.Indented;

            XmlDocument xmldoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmldoc.CreateXmlDeclaration("1.0", "utf-8", null);
            xmldoc.AppendChild(xmlDeclaration);

            XmlElement elemtTranslationContent = xmldoc.CreateElement("TranslationContent");
            
            XmlAttribute attrCT2ProjectId = xmldoc.CreateAttribute("CT2ProjectId");
            attrCT2ProjectId.Value = translationContent.CT2ProjectId;
            elemtTranslationContent.SetAttributeNode(attrCT2ProjectId);

            XmlAttribute attrCT2AssetId = xmldoc.CreateAttribute("CT2AssetId");
            attrCT2AssetId.Value = translationContent.CT2AssetId;
            elemtTranslationContent.SetAttributeNode(attrCT2AssetId);

            XmlAttribute attrCT2SourceLanguageCode = xmldoc.CreateAttribute("CT2SourceLanguageCode");
            attrCT2SourceLanguageCode.Value = translationContent.CT2SourceLanguageCode;
            elemtTranslationContent.SetAttributeNode(attrCT2SourceLanguageCode);

            XmlAttribute attrCT2TargetLanguageCode = xmldoc.CreateAttribute("CT2TargetLanguageCode");
            attrCT2TargetLanguageCode.Value = translationContent.CT2TargetLanguageCode;
            elemtTranslationContent.SetAttributeNode(attrCT2TargetLanguageCode);

            XmlAttribute attrSitecoreSourceLanguageCode = xmldoc.CreateAttribute("SitecoreSourceLanguageCode");
            attrSitecoreSourceLanguageCode.Value = translationContent.SitecoreSourceLanguageCode;
            elemtTranslationContent.SetAttributeNode(attrSitecoreSourceLanguageCode);

            XmlAttribute attrSitecoreTargetLanguageCode = xmldoc.CreateAttribute("SitecoreTargetLanguageCode");
            attrSitecoreTargetLanguageCode.Value = translationContent.SitecoreTargetLanguageCode;
            elemtTranslationContent.SetAttributeNode(attrSitecoreTargetLanguageCode);  

            foreach (SitecoreItem sitecoreItem in translationContent.SitecoreItems)
            {
                XmlElement elmtSitecoreItem = xmldoc.CreateElement("SitecoreItem");

                XmlAttribute attrDatabaseName = xmldoc.CreateAttribute("DatabaseName");
                attrDatabaseName.Value = sitecoreItem.DatabaseName;
                elmtSitecoreItem.SetAttributeNode(attrDatabaseName);

                XmlAttribute attrItemId = xmldoc.CreateAttribute("ItemId");
                attrItemId.Value = sitecoreItem.ItemId;
                elmtSitecoreItem.SetAttributeNode(attrItemId);

                XmlAttribute attrItemSourceVersion = xmldoc.CreateAttribute("ItemSourceVersion");
                attrItemSourceVersion.Value = sitecoreItem.ItemSourceVersion.ToString();
                elmtSitecoreItem.SetAttributeNode(attrItemSourceVersion);

                XmlAttribute attrItemTargetVersion = xmldoc.CreateAttribute("ItemTargetVersion");
                attrItemTargetVersion.Value = sitecoreItem.ItemTargetVersion.ToString();
                elmtSitecoreItem.SetAttributeNode(attrItemTargetVersion);

                XmlAttribute attrTranslationDeadline = xmldoc.CreateAttribute("TranslationDeadline");
                attrTranslationDeadline.Value = sitecoreItem.TranslationDeadline;
                elmtSitecoreItem.SetAttributeNode(attrTranslationDeadline); 

                foreach (ItemField itemField in sitecoreItem.ItemFields)
                {
                    XmlElement elmtFieldContent = xmldoc.CreateElement("FieldContent");
                    XmlAttribute attrFieldName = xmldoc.CreateAttribute("FieldName");
                    attrFieldName.Value = itemField.FieldName;
                    elmtFieldContent.SetAttributeNode(attrFieldName);

                    elmtFieldContent.InnerText = itemField.FieldContent;

                    elmtSitecoreItem.AppendChild(elmtFieldContent);
                }

                elemtTranslationContent.AppendChild(elmtSitecoreItem);
            }


            xmldoc.AppendChild(elemtTranslationContent);

            using (XmlTextWriter xmlWriter = new XmlTextWriter(xmlfileNamePath, new UTF8Encoding(false)))
            {
                xmlWriter.Formatting = Formatting.Indented;

                xmldoc.WriteContentTo(xmlWriter);
                xmlWriter.Flush();
                xmlWriter.Close();
            }

            /*
            xmldoc.WriteContentTo(xmlWriter);
            xmlWriter.Flush(); 
            xmlWriter.Close();
            */

        }

        public static TranslationContent readTranslationContentFromHTMLFile(String htmlfileNamePath)
        {
            /*Sample format
             * 
                <!--CT2Translation:[From:CT2_SourceLanguage To:CT2_TargetLanguage]-->
               
                <!--CT2SharedMeta:CT2ProjectID|CT2AssetID|SC_SourceLanguage|SC_TargetLanguage|CT2_SourceLanguage|CT2_TargetLanguage:SharedMetaEnd-->
             
                <!--CT2FieldMeta:Dababase|ItemID|sourceVersion|targetVersion|FieldName:Begin-->
                Field Content 
                <!--CT2MetaField::End-->
             * 
             */

            if (!System.IO.File.Exists(htmlfileNamePath))
            {
                 CLogger.WriteLog(ELogLevel.ERROR,"readTranslationContentFromHTMLFile Error: passed file doesn't exist.");
                 CLogger.WriteLog(ELogLevel.ERROR,"Passed File: " + htmlfileNamePath);
                return null;

            }
            

            String htmlFileString = com.claytablet.util.FileUtil.ReadStringFromFile(htmlfileNamePath);
            if (String.IsNullOrEmpty(htmlFileString))
            {
                 CLogger.WriteLog(ELogLevel.ERROR,"readTranslationContentFromHTMLFile Error: file is empty");
                 CLogger.WriteLog(ELogLevel.ERROR,"Passed File: " + htmlfileNamePath);
                return null;
            }

            TranslationContent translationContent = new TranslationContent();

            #region first to get shard meta data
            String search_CT2SharedMeta_begin = "<!--CT2SharedMeta:";
            String search_CT2SharedMeta_end = ":SharedMetaEnd-->";

            int pos_CT2SharedMeta_begin = htmlFileString.IndexOf(search_CT2SharedMeta_begin, 0);
            
            if (pos_CT2SharedMeta_begin >= 0)
            {
                int pos_CT2SharedMeta_end = htmlFileString.IndexOf(search_CT2SharedMeta_end, pos_CT2SharedMeta_begin);
                if (pos_CT2SharedMeta_end > pos_CT2SharedMeta_begin + search_CT2SharedMeta_begin.Length)
                {
                    String metaData = htmlFileString.Substring(pos_CT2SharedMeta_begin + search_CT2SharedMeta_begin.Length, pos_CT2SharedMeta_end - pos_CT2SharedMeta_begin - search_CT2SharedMeta_begin.Length);
                    String[] metaArray = metaData.Split('|');
                    if (metaArray.Length == 6)
                    {
                        translationContent.CT2ProjectId = metaArray[0];
                        translationContent.CT2AssetId = metaArray[1];
                        translationContent.SitecoreSourceLanguageCode = metaArray[2];
                        translationContent.SitecoreTargetLanguageCode = metaArray[3];
                        translationContent.CT2SourceLanguageCode = metaArray[4];
                        translationContent.CT2TargetLanguageCode = metaArray[5];
 
                    }
                    else
                        return null;
                }
                else
                   return null;
            }
            else
                return null;

            #endregion first to get shard meta data

            #region then item field content
            String search_CT2FieldMeta_begin = "<!--CT2FieldMeta:";
            String search_CT2FieldMeta_end = ":Begin-->";
            String search_CT2FieldMeta_close = "<!--CT2MetaField::End-->";


            String getDBName = null;
            String getItemId = null;
            int getSourceVersion = 0;
            int getTargetVersion = 0;
            String getFieldName = null;
            String getFieldContent = null;

            Boolean needExit = false;
            while (htmlFileString.IndexOf(search_CT2FieldMeta_begin, 0) >= 0 && !needExit)
            {
                int pos_CT2FieldMeta_begin = htmlFileString.IndexOf(search_CT2FieldMeta_begin, 0);

                int pos_CT2FieldMeta_end = htmlFileString.IndexOf(search_CT2FieldMeta_end, pos_CT2FieldMeta_begin);

                if (pos_CT2FieldMeta_end > pos_CT2FieldMeta_begin + search_CT2FieldMeta_begin.Length)
                {
                    String metaData = htmlFileString.Substring(pos_CT2FieldMeta_begin + search_CT2FieldMeta_begin.Length, pos_CT2FieldMeta_end - pos_CT2FieldMeta_begin - search_CT2FieldMeta_begin.Length);
                    String[] metaArray = metaData.Split('|');
                    
                    if (metaArray.Length == 5)
                    {
                        getDBName = metaArray[0];
                        getItemId = metaArray[1];
                        getSourceVersion = int.Parse(metaArray[2]);
                        getTargetVersion = int.Parse(metaArray[3]);
                        getFieldName = metaArray[4];                         

                        htmlFileString = htmlFileString.Substring(pos_CT2FieldMeta_end + search_CT2FieldMeta_end.Length);

                        int pos_CT2FieldMeta_close = htmlFileString.IndexOf(search_CT2FieldMeta_close, 0);
                        if (pos_CT2FieldMeta_close >= 0)
                        {
                            getFieldContent = cleanContent(htmlFileString.Substring(0, pos_CT2FieldMeta_close));
                            htmlFileString = htmlFileString.Substring(pos_CT2FieldMeta_close + search_CT2FieldMeta_close.Length);

                            SitecoreItem foundSitecoreItem = null;
                            List<SitecoreItem> newSitecoreItemList = new List<SitecoreItem>();

                            //first search translationContent for same SitecoreItem
                            foreach (SitecoreItem sitecoreItem in translationContent.SitecoreItems)
                            {
                                if (String.Compare(sitecoreItem.DatabaseName, getDBName, true) == 0 &&
                                    String.Compare(sitecoreItem.ItemId, getItemId, true) == 0 &&
                                    sitecoreItem.ItemSourceVersion == getSourceVersion &&
                                    sitecoreItem.ItemTargetVersion == getTargetVersion
                                    )
                                {
                                    // found
                                    foundSitecoreItem = sitecoreItem;

                                }
                                else
                                    newSitecoreItemList.Add(sitecoreItem);
                            }

                            if (foundSitecoreItem == null)
                            {
                                foundSitecoreItem = new SitecoreItem();

                                foundSitecoreItem.DatabaseName = getDBName;
                                foundSitecoreItem.ItemId = getItemId;
                                foundSitecoreItem.ItemSourceVersion = getSourceVersion;
                                foundSitecoreItem.ItemTargetVersion = getTargetVersion;
                            }

                            ItemField newItemField = new ItemField();
                            newItemField.FieldName = getFieldName;
                            newItemField.FieldContent = getFieldContent;

                            foundSitecoreItem.AddItemField(newItemField);

                            //put back/add to SitecoreItem list of translationContent
                            newSitecoreItemList.Add(foundSitecoreItem);
                            translationContent.SitecoreItems = newSitecoreItemList;
                        }
                        else
                            needExit = true;
                    }
                    else
                        needExit = true;
 
                }
                else
                    needExit = true;
            }

            #endregion then item field content

            return translationContent;
        }

        public static void writeTranslationContentToHTMLFile(TranslationContent translationContent, String htmlfileNamePath)
        {
            /*Sample format
             * 
                <!--CT2Translation:[From:CT2_SourceLanguage To:CT2_TargetLanguage]-->
               
                <!--CT2SharedMeta:CT2ProjectID|CT2AssetID|SC_SourceLanguage|SC_TargetLanguage|CT2_SourceLanguage|CT2_TargetLanguage:SharedMetaEnd-->
             
                <!--CT2FieldMeta:Dababase|ItemID|sourceVersion|targetVersion|FieldName:Begin-->
                Field Content 
                <!--CT2MetaField::End-->
             * 
             */

            StringBuilder htmlBuilder = new StringBuilder();

            htmlBuilder.AppendFormat("<!--CT2Translation:[From:{0} To:{1}]-->",translationContent.CT2SourceLanguageCode ,translationContent.CT2TargetLanguageCode);
            htmlBuilder.AppendLine();
            htmlBuilder.AppendLine();
            htmlBuilder.AppendFormat("<!--CT2SharedMeta:{0}|{1}|{2}|{3}|{4}|{5}:SharedMetaEnd-->",
                translationContent.CT2ProjectId, translationContent.CT2AssetId,
                translationContent.SitecoreSourceLanguageCode, translationContent.SitecoreTargetLanguageCode,
                translationContent.CT2SourceLanguageCode, translationContent.CT2TargetLanguageCode
                );
            htmlBuilder.AppendLine();
            htmlBuilder.AppendLine();
            foreach (SitecoreItem sitecoreItem in translationContent.SitecoreItems)
                foreach (ItemField itemField in sitecoreItem.ItemFields)
                {
                    htmlBuilder.AppendFormat("<!--CT2FieldMeta:{0}|{1}|{2}|{3}|{4}:Begin-->", sitecoreItem.DatabaseName,
                    sitecoreItem.ItemId, sitecoreItem.ItemSourceVersion, sitecoreItem.ItemTargetVersion, itemField.FieldName);
                  htmlBuilder.AppendLine();
                  htmlBuilder.Append(itemField.FieldContent).AppendLine();
                  htmlBuilder.Append("<!--CT2MetaField::End-->").AppendLine();
                  htmlBuilder.AppendLine();
                }

            com.claytablet.util.FileUtil.WriteStringToFile(htmlfileNamePath, htmlBuilder.ToString()); 

        }

        public static TranslationContent fromXml(String xml)
        {
            try
            {
                // deserialize the object
                return (TranslationContent)getXStream().FromXml(xml);
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Failed to deserialize xml data to a TranslationContent object.");
                CLogger.WriteLog(ELogLevel.ERROR, "XML Data:" + xml);
                CLogger.WriteLog(ELogLevel.ERROR, "Error:" + e.Message);
                return null;
            }
        }

        public static String toXml(TranslationContent translationContent)
        {

            // serilize the object to xml and return it
            return getXStream().ToXml(translationContent);
        }

        private static XStream getXStream()
        {
            XStream xstream = new XStream(); 
            return xstream;
        }

        private static String cleanContent(String content)
        {
            if (content.StartsWith("\r\n") )
            {
                content = content.Substring(2);
            }

            if (content.StartsWith("\n"))
            {
                content = content.Substring(1);
            }

            if (content.EndsWith("\r\n"))
            {
                content = content.Substring(0, content.Length - 2);
            }

            if (content.EndsWith("\n"))
            {
                content = content.Substring(0, content.Length - 1);
            }

            return content;

        }

   }

}
