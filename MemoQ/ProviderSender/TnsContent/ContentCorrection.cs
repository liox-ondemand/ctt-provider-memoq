using System;
using System.Collections.Generic;
using System.Text;

namespace ClayTablet.SC.Translation
{
    [Serializable]
    public class ContentCorrection
    {
        private String m_sourceContent;
        private String m_targetContent; 

        public String SourceContent
        {
            get { return this.m_sourceContent; }
            set { this.m_sourceContent = value; }
        }

        public String TargetContent
        {
            get { return this.m_targetContent; }
            set { this.m_targetContent = value; }
        } 
        
    }
}
