using System;
using System.Collections.Generic;
using System.Text;
using Xstream.Core;
using com.claytablet.util;
using ClayTablet.CT.Utility; 

namespace ClayTablet.SC.Translation
{
    [Serializable]
    public class FieldContentInfo
    {
        private String m_itemDB;
        private String m_itemID;
        private int m_itemVersion;
        private String m_itemLanguage;
        private String m_itemField;
        private String m_itemContent;
        private DateTime m_createTime = DateTime.Now;

        private String m_identityID;

        public FieldContentInfo()
        {
            //m_identityID = com.claytablet.util.IdGenerator.createId();
        }

        public DateTime CreateTime
        {
            get { return this.m_createTime; }
            set { this.m_createTime = value; }
        }

        public int ItemVersion
        {
            get { return this.m_itemVersion; }
            set { this.m_itemVersion = value; }
        }

        public String ItemField
        {
            get { return this.m_itemField; }
            set { this.m_itemField = value; }
        }

        public String ItemContent
        {
            get { return this.m_itemContent; }
            set { this.m_itemContent = value; }
        }

        public String ItemID
        {
            get { return this.m_itemID; }
            set { this.m_itemID = value; }
        }

        public String IdentityID
        {
            get { return this.m_identityID; }
            set { this.m_identityID = value; }
        }

        public String ItemDB
        {
            get { return this.m_itemDB; }
            set { this.m_itemDB = value; }
        }

        public String ItemLanguage
        {
            get { return this.m_itemLanguage; }
            set { this.m_itemLanguage = value; }
        }


        public static FieldContentInfo fromXml(String xml)
        {
            try
            {
                // deserialize the object
                return (FieldContentInfo)getXStream().FromXml(xml); 
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Failed to deserialize xml data to a FieldContentInfo object.");
                CLogger.WriteLog(ELogLevel.ERROR, "XML Data:" + xml);
                CLogger.WriteLog(ELogLevel.ERROR, "Error:" + e.Message);
                return null;
            }
        }

        public static String toXml(FieldContentInfo fieldContentInfo)
        {

            // serilize the object to xml and return it
            return getXStream().ToXml(fieldContentInfo);
        }

        private static XStream getXStream()
        {
            XStream xstream = new XStream();
            xstream.Alias("FieldContentInfo", typeof(FieldContentInfo));
            return xstream;
        } 
 
        
    }
}
