using System;
using System.Collections.Generic;
using System.Text;

namespace ClayTablet.SC.Translation
{
    [Serializable]
    public class TranslationResult
    {
        private bool m_success = false;
        private String m_reference;
        private String m_errorMessage;
        private String m_TranslationFilePath = null;

        public bool Success
        {
            get { return this.m_success; }
            set { this.m_success = value; }
        }

        public String Reference
        {
            get { return this.m_reference; }
            set { this.m_reference = value; }
        }

        public String ErrorMessage
        {
            get { return this.m_errorMessage; }
            set { this.m_errorMessage = value; }
        }

        public String TranslationFilePath
        {
            get { return this.m_TranslationFilePath; }
            set { this.m_TranslationFilePath = value; }
        }

    }
}
