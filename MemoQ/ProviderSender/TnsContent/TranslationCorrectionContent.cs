using System;
using System.Collections.Generic;
using System.Text;
using Xstream.Core;
using System.Xml;
using ClayTablet.CT.Utility; 

namespace ClayTablet.SC.Translation
{
    /* 
     * TranslationCorrectionContent is the class to hold the information for TNS to correct the translation
     * 
     * Each TranslationCorrectionContent is based on one sitecore item, with content from multiple fields
     *  
     * 
     */
    [Serializable]
    public class TranslationCorrectionContent
    {
        private String m_ct2ProjectId;   //CT2 Project ID
        private String m_ct2AssetId;   //CT2 Asset ID 
        private String m_sitecoreSourceLanguageCode;   //SC Source Language ID 
        private String m_ct2SourceLanguageCode;   //CT2 Language Code
        private String m_sitecoreTargetLanguageCode;   //SC Target Language Code 
        private String m_ct2TargetLanguageCode;   //CT2 Target Language Code
        private String m_databaseName;   //database ID
        private String m_itemId;         //SC Item ID 
        private int m_itemSourceVersion;       //SC Source Version number
        private int m_itemTargetVersion;       //SC Target Version number 
        private String m_notes = "";

        private List<ItemFieldPair> m_itemFields = new List<ItemFieldPair>();

        public String Notes
        {
            get { return this.m_notes; }
            set { this.m_notes = value; }
        }

        public String DatabaseName
        {
            get { return this.m_databaseName; }
            set { this.m_databaseName = value; }
        }

        public int ItemFieldsCount
        {
            get { return this.m_itemFields.Count; }
        }

        public String ItemId
        {
            get { return this.m_itemId; }
            set { this.m_itemId = value; }
        }
         
      
        public int ItemSourceVersion
        {
            get { return this.m_itemSourceVersion; }
            set { this.m_itemSourceVersion = value; }
        }
        

        public int ItemTargetVersion
        {
            get { return this.m_itemTargetVersion; }
            set { this.m_itemTargetVersion = value; }
        }

        public List<ItemFieldPair> ItemFields
        {
            get { return this.m_itemFields; }
            set { this.m_itemFields = value; }
        }

        public void AddItemField(ItemFieldPair itemFieldPair)
        {
            m_itemFields.Add(itemFieldPair);
        }

        public String CT2ProjectId
        {
            get { return this.m_ct2ProjectId; }
            set { this.m_ct2ProjectId = value; }
        }

        public String CT2AssetId
         {
             get { return this.m_ct2AssetId; }
             set { this.m_ct2AssetId = value; }
         }

        public String SitecoreSourceLanguageCode
         {
             get { return this.m_sitecoreSourceLanguageCode; }
             set { this.m_sitecoreSourceLanguageCode = value; }
         }

        public String CT2SourceLanguageCode
         {
             get { return this.m_ct2SourceLanguageCode; }
             set { this.m_ct2SourceLanguageCode = value; }
         }

        public String SitecoreTargetLanguageCode
        {
            get { return this.m_sitecoreTargetLanguageCode; }
            set { this.m_sitecoreTargetLanguageCode = value; }
        }

        public String CT2TargetLanguageCode
        {
            get { return this.m_ct2TargetLanguageCode; }
            set { this.m_ct2TargetLanguageCode = value; }
        }


        public static TranslationCorrectionContent readContentFromXMLFile(String xmlfileNamePath)
        {
            if (!System.IO.File.Exists(xmlfileNamePath) )
                return null;

            TranslationCorrectionContent translationCorrectionContent = new TranslationCorrectionContent();

            XmlDocument xmldoc = new XmlDocument();
           // xmldoc.LoadXml(com.claytablet.util.FileUtil.ReadStringFromFile(xmlfileNamePath));
            xmldoc.Load(xmlfileNamePath);

            XmlNodeList xmlContentNodeList = xmldoc.GetElementsByTagName("TranslationCorrectionContent");
            if (xmlContentNodeList.Count != 1)
            {
                return null;
            }

            XmlNode xmlContentNode = xmlContentNodeList[0];
            XmlAttributeCollection xmlattrc = xmlContentNode.Attributes;
            foreach (XmlAttribute xmlAttribute in xmlattrc)
            {
                if (String.Compare(xmlAttribute.Name, "CT2ProjectId", true) == 0)
                    translationCorrectionContent.CT2ProjectId = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "CT2AssetId", true) == 0)
                    translationCorrectionContent.CT2AssetId = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "CT2SourceLanguageCode", true) == 0)
                    translationCorrectionContent.CT2SourceLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "CT2TargetLanguageCode", true) == 0)
                    translationCorrectionContent.CT2TargetLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "SitecoreSourceLanguageCode", true) == 0)
                    translationCorrectionContent.SitecoreSourceLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "SitecoreTargetLanguageCode", true) == 0)
                    translationCorrectionContent.SitecoreTargetLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "DatabaseName", true) == 0)
                    translationCorrectionContent.DatabaseName = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "ItemId", true) == 0)
                    translationCorrectionContent.ItemId = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "ItemSourceVersion", true) == 0)
                    translationCorrectionContent.ItemSourceVersion = int.Parse(xmlAttribute.Value);
                else if (String.Compare(xmlAttribute.Name, "ItemTargetVersion", true) == 0)
                    translationCorrectionContent.ItemTargetVersion = int.Parse(xmlAttribute.Value);
                 

            }

            XmlNodeList xmlSiteItemNodeList = xmldoc.GetElementsByTagName("ItemField");
            foreach (XmlNode xmlNode in xmlSiteItemNodeList)
            {

                ItemFieldPair newItemField = new ItemFieldPair();                

                #region get filename
                XmlAttributeCollection xmlattrc_Item = xmlNode.Attributes;
                foreach (XmlAttribute xmlAttribute in xmlattrc_Item)
                {
                    if (String.Compare(xmlAttribute.Name, "FieldName", true) == 0)
                        newItemField.FieldName = xmlAttribute.Value;

                }
                #endregion get filename

                #region get Item Fields
                XmlNodeList fieldsNoteList = xmlNode.ChildNodes;
                foreach (XmlNode fieldNode in fieldsNoteList)
                {
                    if (String.Compare(fieldNode.Name, "SourceContent", true) == 0)
                    {
                        newItemField.FieldContent_Source = fieldNode.InnerText;                         
                    }
                    else if (String.Compare(fieldNode.Name, "TargetContent", true) == 0)
                    {
                        newItemField.FieldContent_Target = fieldNode.InnerText;
                    }
                }

                #endregion get Item Fields

                translationCorrectionContent.AddItemField(newItemField);
            }

            return translationCorrectionContent;
        }
      
        public static void writeContentToXMLFile(TranslationCorrectionContent translationCorrectionContent, String xmlfileNamePath)
        {
             

            //XmlTextWriter xmlWriter = new XmlTextWriter(xmlfileNamePath, System.Text.Encoding.UTF8);
            XmlTextWriter xmlWriter = new XmlTextWriter(xmlfileNamePath, new UTF8Encoding(false)); 
            xmlWriter.Formatting = Formatting.Indented;

            XmlDocument xmldoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmldoc.CreateXmlDeclaration("1.0", "utf-8", null);
            xmldoc.AppendChild(xmlDeclaration);

            XmlElement elemtTranslationContent = xmldoc.CreateElement("TranslationCorrectionContent");
            
            XmlAttribute attrCT2ProjectId = xmldoc.CreateAttribute("CT2ProjectId");
            attrCT2ProjectId.Value = translationCorrectionContent.CT2ProjectId;
            elemtTranslationContent.SetAttributeNode(attrCT2ProjectId);

            XmlAttribute attrCT2AssetId = xmldoc.CreateAttribute("CT2AssetId");
            attrCT2AssetId.Value = translationCorrectionContent.CT2AssetId;
            elemtTranslationContent.SetAttributeNode(attrCT2AssetId);

            XmlAttribute attrCT2SourceLanguageCode = xmldoc.CreateAttribute("CT2SourceLanguageCode");
            attrCT2SourceLanguageCode.Value = translationCorrectionContent.CT2SourceLanguageCode;
            elemtTranslationContent.SetAttributeNode(attrCT2SourceLanguageCode);

            XmlAttribute attrCT2TargetLanguageCode = xmldoc.CreateAttribute("CT2TargetLanguageCode");
            attrCT2TargetLanguageCode.Value = translationCorrectionContent.CT2TargetLanguageCode;
            elemtTranslationContent.SetAttributeNode(attrCT2TargetLanguageCode);

            XmlAttribute attrSitecoreSourceLanguageCode = xmldoc.CreateAttribute("SitecoreSourceLanguageCode");
            attrSitecoreSourceLanguageCode.Value = translationCorrectionContent.SitecoreSourceLanguageCode;
            elemtTranslationContent.SetAttributeNode(attrSitecoreSourceLanguageCode);

            XmlAttribute attrSitecoreTargetLanguageCode = xmldoc.CreateAttribute("SitecoreTargetLanguageCode");
            attrSitecoreTargetLanguageCode.Value = translationCorrectionContent.SitecoreTargetLanguageCode;
            elemtTranslationContent.SetAttributeNode(attrSitecoreTargetLanguageCode); 
 
            XmlAttribute attrDatabaseName = xmldoc.CreateAttribute("DatabaseName");
            attrDatabaseName.Value = translationCorrectionContent.DatabaseName;
            elemtTranslationContent.SetAttributeNode(attrDatabaseName); 

            XmlAttribute attrItemId = xmldoc.CreateAttribute("ItemId");
            attrItemId.Value = translationCorrectionContent.ItemId;
            elemtTranslationContent.SetAttributeNode(attrItemId);

            XmlAttribute attrItemSourceVersion = xmldoc.CreateAttribute("ItemSourceVersion");
            attrItemSourceVersion.Value = translationCorrectionContent.ItemSourceVersion.ToString();
            elemtTranslationContent.SetAttributeNode(attrItemSourceVersion);

            XmlAttribute attrItemTargetVersion = xmldoc.CreateAttribute("ItemTargetVersion");
            attrItemTargetVersion.Value = translationCorrectionContent.ItemTargetVersion.ToString();
            elemtTranslationContent.SetAttributeNode(attrItemTargetVersion);

            XmlElement elmtNotes = xmldoc.CreateElement("Notes");
            elmtNotes.InnerText = "";
            if (!String.IsNullOrEmpty(translationCorrectionContent.Notes)) 
                elmtNotes.InnerText = translationCorrectionContent.Notes;
            elemtTranslationContent.AppendChild(elmtNotes);

            XmlElement elmtSitecoreItem = xmldoc.CreateElement("ItemFields");

            foreach (ItemFieldPair itemField  in translationCorrectionContent.ItemFields)
            {
                XmlElement elmtSitecoreItemField = xmldoc.CreateElement("ItemField");

                XmlAttribute attrFieldName = xmldoc.CreateAttribute("FieldName");
                attrFieldName.Value = itemField.FieldName;
                elmtSitecoreItemField.SetAttributeNode(attrFieldName);

                XmlElement elmtSourceContent = xmldoc.CreateElement("SourceContent");
                elmtSourceContent.InnerText = itemField.FieldContent_Source;
                elmtSitecoreItemField.AppendChild(elmtSourceContent);

                XmlElement elmtTargetContent = xmldoc.CreateElement("TargetContent");
                elmtTargetContent.InnerText = itemField.FieldContent_Target;
                elmtSitecoreItemField.AppendChild(elmtTargetContent);

                elmtSitecoreItem.AppendChild(elmtSitecoreItemField);
            }
            elemtTranslationContent.AppendChild(elmtSitecoreItem);

            xmldoc.AppendChild(elemtTranslationContent);  

            xmldoc.WriteContentTo(xmlWriter);
            xmlWriter.Flush(); 
            xmlWriter.Close();

        }

        public static TranslationCorrectionContent readContentFromHTMLFile(String htmlfileNamePath)
        {
            /*Sample format
             * 
                <!--CT2TranslationCorrection:[From:CT2SourceLanguage To:CT2TargetLanguage]-->
             
                 <!--CT2SharedMeta:CT2ProjectID|CT2AssetID|SC_SourceLanguage|SC_TargetLanguage|CT2_SourceLanguage|CT2_TargetLanguage:SharedMetaEnd-->
             
                <!--CT2ItemMeta:Dababase|ItemID|sourceVersion|targetVersion:ItemMetaEnd--> 

                <!--CT2Note::Begin--> 
                Some translation are not correct...
                <!--CT2Note::End-->

                <!--CT2FieldMeta:FieldName:Begin-->

                <!--Source Content::Begin-->
                Brazil flood toll exceeds 100
                <!--Source Content::End-->

                <!--Translated Content::Begin-->
                Sans les inondations au Br�sil d�passe les 100
                <!--Translated Content::End-->

                <!--CT2MetaField::End-->
             * 
             */

            if (!System.IO.File.Exists(htmlfileNamePath))
                return null;


            String htmlFileString = com.claytablet.util.FileUtil.ReadStringFromFile(htmlfileNamePath);
            if (String.IsNullOrEmpty(htmlFileString))
                return null;

            TranslationCorrectionContent translationCorrectionContent = new TranslationCorrectionContent();

            #region first to get shard meta data
            String search_CT2SharedMeta_begin = "<!--CT2SharedMeta:";
            String search_CT2SharedMeta_end = ":SharedMetaEnd-->";

            int pos_CT2SharedMeta_begin = htmlFileString.IndexOf(search_CT2SharedMeta_begin, 0);

            if (pos_CT2SharedMeta_begin >= 0)
            {
                int pos_CT2SharedMeta_end = htmlFileString.IndexOf(search_CT2SharedMeta_end, pos_CT2SharedMeta_begin);
                if (pos_CT2SharedMeta_end > pos_CT2SharedMeta_begin + search_CT2SharedMeta_begin.Length)
                {
                    String metaData = htmlFileString.Substring(pos_CT2SharedMeta_begin + search_CT2SharedMeta_begin.Length, pos_CT2SharedMeta_end - pos_CT2SharedMeta_begin - search_CT2SharedMeta_begin.Length);
                    String[] metaArray = metaData.Split('|');
                    if (metaArray.Length == 6)
                    {
                        translationCorrectionContent.CT2ProjectId = metaArray[0];
                        translationCorrectionContent.CT2AssetId = metaArray[1];
                        translationCorrectionContent.SitecoreSourceLanguageCode = metaArray[2];
                        translationCorrectionContent.SitecoreTargetLanguageCode = metaArray[3];
                        translationCorrectionContent.CT2SourceLanguageCode = metaArray[4];
                        translationCorrectionContent.CT2TargetLanguageCode = metaArray[5]; 
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            else
                return null;

            #endregion first to get shard meta data

            #region second, to get Item meta data
            String search_CT2ItemMeta_begin = "<!--CT2ItemMeta:";
            String search_CT2ItemMeta_end = ":ItemMetaEnd-->";

            int pos_CT2ItemMeta_begin = htmlFileString.IndexOf(search_CT2ItemMeta_begin, 0);

            if (pos_CT2ItemMeta_begin >= 0)
            {
                int pos_CT2ItemMeta_end = htmlFileString.IndexOf(search_CT2ItemMeta_end, pos_CT2ItemMeta_begin);
                if (pos_CT2ItemMeta_end > pos_CT2ItemMeta_begin + search_CT2ItemMeta_begin.Length)
                {
                    String metaData = htmlFileString.Substring(pos_CT2ItemMeta_begin + search_CT2ItemMeta_begin.Length, pos_CT2ItemMeta_end - pos_CT2ItemMeta_begin - search_CT2ItemMeta_begin.Length);
                    String[] metaArray = metaData.Split('|');
                    if (metaArray.Length == 4)
                    {
                        translationCorrectionContent.DatabaseName = metaArray[0];
                        translationCorrectionContent.ItemId = metaArray[1];
                        translationCorrectionContent.ItemSourceVersion = int.Parse(metaArray[2]);
                        translationCorrectionContent.ItemTargetVersion = int.Parse(metaArray[3]);   
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            else
                return null;

            #endregion second, to get Item meta data


            #region third to Notes
            String search_CT2NotesMeta_begin = "<!--CT2Note::Begin-->";
            String search_CT2NotesMeta_end = "<!--CT2Note::End-->";

            int pos_CT2NotesMeta_begin = htmlFileString.IndexOf(search_CT2NotesMeta_begin, 0);

            if (pos_CT2NotesMeta_begin >= 0)
            {
                int pos_CT2NotesMeta_end = htmlFileString.IndexOf(search_CT2NotesMeta_end, pos_CT2NotesMeta_begin);
                if (pos_CT2NotesMeta_end > pos_CT2NotesMeta_begin + search_CT2NotesMeta_begin.Length)
                {
                    translationCorrectionContent.Notes = cleanContent(htmlFileString.Substring(pos_CT2NotesMeta_begin + search_CT2NotesMeta_begin.Length, pos_CT2NotesMeta_end - pos_CT2NotesMeta_begin - search_CT2NotesMeta_begin.Length) );
                 
                }
                 
            }        

            #endregion third to Notes

            #region then item field content

            String search_CT2FieldMeta_begin = "<!--CT2FieldMeta:";
            String search_CT2FieldMeta_end = ":Begin-->";
            String search_CT2FieldMeta_close = "<!--CT2MetaField::End-->";

            String search_CT2SourceMeta_begin = "<!--Source Content::Begin-->";
            String search_CT2SourceMeta_end = "<!--Source Content::End-->";
            String search_CT2TargetMeta_begin = "<!--Translated Content::Begin-->";
            String search_CT2TargetMeta_end = "<!--Translated Content::End-->";

            

            Boolean needExit = false;
            while (htmlFileString.IndexOf(search_CT2FieldMeta_begin, 0) >= 0 && !needExit)
            {
                int pos_CT2FieldMeta_begin = htmlFileString.IndexOf(search_CT2FieldMeta_begin, 0);

                int pos_CT2FieldMeta_end = htmlFileString.IndexOf(search_CT2FieldMeta_end, pos_CT2FieldMeta_begin);

                if (pos_CT2FieldMeta_end > pos_CT2FieldMeta_begin + search_CT2FieldMeta_begin.Length)
                {
                    String getFieldName = htmlFileString.Substring(pos_CT2FieldMeta_begin + search_CT2FieldMeta_begin.Length, pos_CT2FieldMeta_end - pos_CT2FieldMeta_begin - search_CT2FieldMeta_begin.Length);
                    
                    int pos_CT2FieldMeta_close = htmlFileString.IndexOf(search_CT2FieldMeta_close, 0); 
                    if (pos_CT2FieldMeta_close >= 0)
                    {
                        String getBothFieldContent = htmlFileString.Substring(0, pos_CT2FieldMeta_close);
                        htmlFileString = htmlFileString.Substring(pos_CT2FieldMeta_close + search_CT2FieldMeta_close.Length);


                        int pos_CT2SourceMeta_begin = getBothFieldContent.IndexOf(search_CT2SourceMeta_begin, 0);
                        int pos_CT2SourceMeta_end = getBothFieldContent.IndexOf(search_CT2SourceMeta_end, 0);

                        int pos_CT2TargetMeta_begin = getBothFieldContent.IndexOf(search_CT2TargetMeta_begin, 0);
                        int pos_CT2TargetMeta_end = getBothFieldContent.IndexOf(search_CT2TargetMeta_end, 0);

                        if (pos_CT2SourceMeta_begin > 0 && pos_CT2SourceMeta_end > (pos_CT2SourceMeta_begin + search_CT2SourceMeta_begin.Length) &&
                            pos_CT2TargetMeta_begin > 0 && pos_CT2TargetMeta_end > (pos_CT2TargetMeta_begin + search_CT2TargetMeta_begin.Length)
                            )
                        {
                            ItemFieldPair newItemFieldPair = new ItemFieldPair();

                            newItemFieldPair.FieldName = getFieldName;
                            newItemFieldPair.FieldContent_Source = cleanContent(getBothFieldContent.Substring(pos_CT2SourceMeta_begin + search_CT2SourceMeta_begin.Length, pos_CT2SourceMeta_end - pos_CT2SourceMeta_begin - search_CT2SourceMeta_begin.Length));
                            newItemFieldPair.FieldContent_Target = cleanContent(getBothFieldContent.Substring(pos_CT2TargetMeta_begin + search_CT2TargetMeta_begin.Length, pos_CT2TargetMeta_end - pos_CT2TargetMeta_begin - search_CT2TargetMeta_begin.Length));

                            translationCorrectionContent.AddItemField(newItemFieldPair);

                        }
                         
                    }
                    else
                        needExit = true;                     

                }
                else
                    needExit = true;
            }

            #endregion then item field content

            return translationCorrectionContent;
        }

        public static void writeContentToHTMLFile(TranslationCorrectionContent translationCorrectionContent, String htmlfileNamePath)
        {
            /*Sample format
             * 
                <!--CT2TranslationCorrection:[From:CT2SourceLanguage To:CT2TargetLanguage]-->
             
                 <!--CT2SharedMeta:CT2ProjectID|CT2AssetID|SC_SourceLanguage|SC_TargetLanguage|CT2_SourceLanguage|CT2_TargetLanguage:SharedMetaEnd-->
             
                <!--CT2ItemMeta:Dababase|ItemID|sourceVersion|targetVersion:ItemMetaEnd--> 

                <!--CT2Note::Begin--> 
                Some translation are not correct...
                <!--CT2Note::End-->

                <!--CT2FieldMeta:FieldName:Begin-->

                <!--Source Content::Begin-->
                Brazil flood toll exceeds 100
                <!--Source Content::End-->

                <!--Translated Content::Begin-->
                Sans les inondations au Br�sil d�passe les 100
                <!--Translated Content::End-->

                <!--CT2MetaField::End-->
             * 
             */

            StringBuilder htmlBuilder = new StringBuilder();

            htmlBuilder.AppendFormat("<!--CT2TranslationCorrection:[From:{0} To:{1}]-->", translationCorrectionContent.CT2SourceLanguageCode, translationCorrectionContent.CT2TargetLanguageCode);
            htmlBuilder.AppendLine();
            htmlBuilder.AppendLine();
            
            htmlBuilder.AppendFormat("<!--CT2SharedMeta:{0}|{1}|{2}|{3}|{4}|{5}:SharedMetaEnd-->",
                translationCorrectionContent.CT2ProjectId, translationCorrectionContent.CT2AssetId,
                translationCorrectionContent.SitecoreSourceLanguageCode, translationCorrectionContent.SitecoreTargetLanguageCode,
                translationCorrectionContent.CT2SourceLanguageCode, translationCorrectionContent.CT2TargetLanguageCode
                );
            htmlBuilder.AppendLine();
            htmlBuilder.AppendLine();

            htmlBuilder.AppendFormat("<!--CT2ItemMeta:{0}|{1}|{2}|{3}:ItemMetaEnd-->",
                translationCorrectionContent.DatabaseName, translationCorrectionContent.ItemId,
                translationCorrectionContent.ItemSourceVersion, translationCorrectionContent.ItemTargetVersion 
                );
            htmlBuilder.AppendLine();
            htmlBuilder.AppendLine();

            if (!String.IsNullOrEmpty(translationCorrectionContent.Notes))
            {

                htmlBuilder.Append("<!--CT2Note::Begin-->").AppendLine();
                htmlBuilder.Append(translationCorrectionContent.Notes).AppendLine();
                htmlBuilder.Append("<!--CT2Note::End-->").AppendLine();
                htmlBuilder.AppendLine();

            }


            foreach (ItemFieldPair itemFieldPair in translationCorrectionContent.ItemFields) 
            {
                htmlBuilder.AppendFormat("<!--CT2FieldMeta:{0}:Begin-->", itemFieldPair.FieldName).AppendLine();
                htmlBuilder.AppendLine();
                htmlBuilder.Append("<!--Source Content::Begin-->").AppendLine();
                htmlBuilder.Append(itemFieldPair.FieldContent_Source).AppendLine();
                htmlBuilder.Append("<!--Source Content::End-->").AppendLine();
                htmlBuilder.AppendLine();
                htmlBuilder.Append("<!--Translated Content::Begin-->").AppendLine();
                htmlBuilder.Append(itemFieldPair.FieldContent_Target).AppendLine();
                htmlBuilder.Append("<!--Translated Content::End-->").AppendLine(); 
                htmlBuilder.AppendLine();
                htmlBuilder.Append("<!--CT2MetaField::End-->").AppendLine();
                htmlBuilder.AppendLine();
            }

            com.claytablet.util.FileUtil.WriteStringToFile(htmlfileNamePath, htmlBuilder.ToString());

        }

        private static String cleanContent(String content)
        {
            if (content.StartsWith("\r\n") )
            {
                content = content.Substring(2);
            }

            if (content.StartsWith("\n"))
            {
                content = content.Substring(1);
            }

            if (content.EndsWith("\r\n"))
            {
                content = content.Substring(0, content.Length - 2);
            }

            if (content.EndsWith("\n"))
            {
                content = content.Substring(0, content.Length - 1);
            }

            return content;

        }

        public static TranslationCorrectionContent fromXml(String xml)
        {
            try
            {
               
              return (TranslationCorrectionContent)getXStream().FromXml(xml);
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Failed to deserialize xml data to a TranslationCorrectionContent object.");
                CLogger.WriteLog(ELogLevel.ERROR, "XML Data:" + xml);
                CLogger.WriteLog(ELogLevel.ERROR, "Error:" + e.Message);
                return null;
            }
        }

        public static String toXml(TranslationCorrectionContent translationContent)
        {

            // serilize the object to xml and return it
            return getXStream().ToXml(translationContent);
        }

        private static XStream getXStream()
        {
            XStream xstream = new XStream(); 
            return xstream;
        } 


   }

}
