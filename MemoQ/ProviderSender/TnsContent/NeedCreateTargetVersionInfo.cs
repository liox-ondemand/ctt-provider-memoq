using System;
using System.Collections.Generic;
using System.Text;

namespace ClayTablet.SC.Translation
{
    [Serializable]
    public class NeedCreateTargetVersionInfo : TargetVersionInfo
    {

        protected int m_createType; 
        public NeedCreateTargetVersionInfo()
        {
             
        }

        public NeedCreateTargetVersionInfo(String targetLanguage, int targetVersion, int createType) 
        {
            m_targetLanguage = targetLanguage;
            m_targetVersion = targetVersion;
            m_createType = createType;
        }

        public int CreateType
        {
            get { return this.m_createType; }
            set { this.m_createType = value; }
        }
    }
}
