using System;
using System.Collections.Generic;
using System.Text;
using Xstream.Core;
using System.Xml;
using ClayTablet.CT.Utility; 

namespace ClayTablet.SC.Translation
{
    /* 
     * UpdateTMContent is the class to hold the corrected translation, it will be sent out to TNS,
     * to update the Translation Memory of TNS 
     * 
     */
    [Serializable]
    public class UpdateTMContent
    {

        private String m_sitecoreSourceLanguageCode;   //SC Source Language ID 
        private String m_ct2SourceLanguageCode;   //CT2 Language Code
        private String m_sitecoreTargetLanguageCode;   //SC Target Language Code 
        private String m_ct2TargetLanguageCode;   //CT2 Target Language Code     


        private List<ContentCorrection> m_contentCorrections = new List<ContentCorrection>();


        public List<ContentCorrection> ContentCorrections
        {
            get { return this.m_contentCorrections; }
            set { this.m_contentCorrections = value; }
        }

        public void AddContentCorrection(ContentCorrection contentCorrection)
        {
            m_contentCorrections.Add(contentCorrection);
        } 

        public String SitecoreSourceLanguageCode
         {
             get { return this.m_sitecoreSourceLanguageCode; }
             set { this.m_sitecoreSourceLanguageCode = value; }
         }

        public String CT2SourceLanguageCode
         {
             get { return this.m_ct2SourceLanguageCode; }
             set { this.m_ct2SourceLanguageCode = value; }
         }

        public String SitecoreTargetLanguageCode
        {
            get { return this.m_sitecoreTargetLanguageCode; }
            set { this.m_sitecoreTargetLanguageCode = value; }
        }

        public String CT2TargetLanguageCode
        {
            get { return this.m_ct2TargetLanguageCode; }
            set { this.m_ct2TargetLanguageCode = value; }
        }


        public static UpdateTMContent readContentFromXMLFile(String xmlfileNamePath)
        {
            if (!System.IO.File.Exists(xmlfileNamePath) )
                return null;

            UpdateTMContent updateTMContent = new UpdateTMContent();

            XmlDocument xmldoc = new XmlDocument();
            //xmldoc.LoadXml(com.claytablet.util.FileUtil.ReadStringFromFile(xmlfileNamePath));
            xmldoc.Load(xmlfileNamePath);

            XmlNodeList xmlContentNodeList = xmldoc.GetElementsByTagName("UpdateTMContent");
            if (xmlContentNodeList.Count != 1)
            {
                return null;
            }

            XmlNode xmlContentNode = xmlContentNodeList[0];
            XmlAttributeCollection xmlattrc = xmlContentNode.Attributes;
            foreach (XmlAttribute xmlAttribute in xmlattrc)
            {

                if (String.Compare(xmlAttribute.Name, "CT2SourceLanguageCode", true) == 0)
                    updateTMContent.CT2SourceLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "CT2TargetLanguageCode", true) == 0)
                    updateTMContent.CT2TargetLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "SitecoreSourceLanguageCode", true) == 0)
                    updateTMContent.SitecoreSourceLanguageCode = xmlAttribute.Value;
                else if (String.Compare(xmlAttribute.Name, "SitecoreTargetLanguageCode", true) == 0)
                    updateTMContent.SitecoreTargetLanguageCode = xmlAttribute.Value;                 

            }

            XmlNodeList xmlSiteItemNodeList = xmldoc.GetElementsByTagName("ContentCorrection");
            foreach (XmlNode xmlNode in xmlSiteItemNodeList)
            {

                ContentCorrection newContentCorrection = new ContentCorrection();

                #region get ContentCorrection
                XmlNodeList fieldsNoteList = xmlNode.ChildNodes;
                foreach (XmlNode fieldNode in fieldsNoteList)
                {
                    if (String.Compare(fieldNode.Name, "SourceContent", true) == 0)
                    {
                        newContentCorrection.SourceContent = fieldNode.InnerText;                         
                    }
                    else if (String.Compare(fieldNode.Name, "TargetContent", true) == 0)
                    {
                        newContentCorrection.TargetContent = fieldNode.InnerText;
                    }
                }

                #endregion get ContentCorrection

                updateTMContent.AddContentCorrection(newContentCorrection);
            }

            return updateTMContent;
        }



        public static void writeContentToXMLFile(UpdateTMContent updateTMContent, String xmlfileNamePath)
        {
             

            //XmlTextWriter xmlWriter = new XmlTextWriter(xmlfileNamePath, System.Text.Encoding.UTF8);
            XmlTextWriter xmlWriter = new XmlTextWriter(xmlfileNamePath, new UTF8Encoding(false)); 
            xmlWriter.Formatting = Formatting.Indented;

            XmlDocument xmldoc = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmldoc.CreateXmlDeclaration("1.0", "utf-8", null);
            xmldoc.AppendChild(xmlDeclaration);

            XmlElement elemtUpdateTMContent = xmldoc.CreateElement("UpdateTMContent");

            XmlAttribute attrCT2SourceLanguageCode = xmldoc.CreateAttribute("CT2SourceLanguageCode");
            attrCT2SourceLanguageCode.Value = updateTMContent.CT2SourceLanguageCode;
            elemtUpdateTMContent.SetAttributeNode(attrCT2SourceLanguageCode);

            XmlAttribute attrCT2TargetLanguageCode = xmldoc.CreateAttribute("CT2TargetLanguageCode");
            attrCT2TargetLanguageCode.Value = updateTMContent.CT2TargetLanguageCode;
            elemtUpdateTMContent.SetAttributeNode(attrCT2TargetLanguageCode);

            XmlAttribute attrSitecoreSourceLanguageCode = xmldoc.CreateAttribute("SitecoreSourceLanguageCode");
            attrSitecoreSourceLanguageCode.Value = updateTMContent.SitecoreSourceLanguageCode;
            elemtUpdateTMContent.SetAttributeNode(attrSitecoreSourceLanguageCode);

            XmlAttribute attrSitecoreTargetLanguageCode = xmldoc.CreateAttribute("SitecoreTargetLanguageCode");
            attrSitecoreTargetLanguageCode.Value = updateTMContent.SitecoreTargetLanguageCode;
            elemtUpdateTMContent.SetAttributeNode(attrSitecoreTargetLanguageCode);


            XmlElement elmtContentCorrectionGroup = xmldoc.CreateElement("ContentCorrections");

            foreach (ContentCorrection contentCorrection in updateTMContent.ContentCorrections)
            {
                XmlElement elmtContentCorrection = xmldoc.CreateElement("ContentCorrection");

                XmlElement elmtSourceContent = xmldoc.CreateElement("SourceContent");
                elmtSourceContent.InnerText = contentCorrection.SourceContent;
                elmtContentCorrection.AppendChild(elmtSourceContent);

                XmlElement elmtTargetContent = xmldoc.CreateElement("TargetContent");
                elmtTargetContent.InnerText = contentCorrection.TargetContent;
                elmtContentCorrection.AppendChild(elmtTargetContent);

                elmtContentCorrectionGroup.AppendChild(elmtContentCorrection);
            }
            elemtUpdateTMContent.AppendChild(elmtContentCorrectionGroup);

            xmldoc.AppendChild(elemtUpdateTMContent);  

            xmldoc.WriteContentTo(xmlWriter);
            xmlWriter.Flush(); 
            xmlWriter.Close();

        }

        public static UpdateTMContent fromXml(String xml)
        {
            try
            {
                // deserialize the object
                return (UpdateTMContent)getXStream().FromXml(xml);
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Failed to deserialize xml data to a UpdateTMContent object.");
                CLogger.WriteLog(ELogLevel.ERROR, "XML Data:" + xml);
                CLogger.WriteLog(ELogLevel.ERROR, "Error:" + e.Message);
                return null;
            }

        }

        public static String toXml(UpdateTMContent updateTMContent)
        {

            // serilize the object to xml and return it
            return getXStream().ToXml(updateTMContent);
        }

        private static XStream getXStream()
        {
            XStream xstream = new XStream(); 
            return xstream;
        } 


   }

}
