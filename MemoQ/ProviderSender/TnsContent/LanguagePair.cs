﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClayTablet.SC.Translation
{
    
    [Serializable]
    public class LanguagePair
    { 
        private String m_SourceLanguage;
        private String m_TargetLanguage; 

        public String SourceLanguage
        {
            get { return this.m_SourceLanguage; }
            set { this.m_SourceLanguage = value; }
        }

        public String TargetLanguage
        {
            get { return this.m_TargetLanguage; }
            set { this.m_TargetLanguage = value; }
        }

    }
}
