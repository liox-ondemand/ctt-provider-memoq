using System;
using System.Collections.Generic;
using System.Text;

namespace ClayTablet.SC.Translation
{
    /*  SitecoreItem is the class to hold sitecore item information for TranslationContent
     * 
     * 
     */
    [Serializable]
    public class SitecoreItem
    {
        private String m_databaseName;   //database ID
        private String m_itemId;         //SC Item ID 
        private int m_itemSourceVersion;       //SC Source Version number
        private int m_itemTargetVersion;       //SC Target Version number
        private String m_translationDeadline;

        private List<ItemField> m_itemFields = new List<ItemField>();

        public String DatabaseName
        {
            get { return this.m_databaseName; }
            set { this.m_databaseName = value; }
        }

        public int ItemFieldsCount
        {
            get { return this.m_itemFields.Count; } 
        }

        public String ItemId
        {
            get { return this.m_itemId; }
            set { this.m_itemId = value; }
        }

        public String TranslationDeadline
        {
            get { return this.m_translationDeadline; }
            set { this.m_translationDeadline = value; }
        }

        
        public int ItemSourceVersion
        {
            get { return this.m_itemSourceVersion; }
            set { this.m_itemSourceVersion = value; }
        } 

        public int ItemTargetVersion
        {
            get { return this.m_itemTargetVersion; }
            set { this.m_itemTargetVersion = value; }
        }

        public List<ItemField> ItemFields
        {
            get { return this.m_itemFields; }
            set { this.m_itemFields = value; }
        } 
         
        public void AddItemField(ItemField  itemField)
        {
            m_itemFields.Add(itemField);
        }
    }
}
