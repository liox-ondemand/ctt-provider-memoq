using System;
using System.Collections.Generic;
using System.Text;

namespace ClayTablet.SC.Translation
{
    /*  ItemField is the class to hold content for translation from sitecore item field, 
     *  
     *  TranslationContent can include multiple sitecore items, and each item can include multiple fields
     *  for trnalstion
     * 
     * 
     */
    [Serializable]
    public class ItemField
    {
        private String m_fieldName;
        private String m_fieldContent; 

        public String FieldName
        {
            get { return this.m_fieldName; }
            set { this.m_fieldName = value; }
        }

        public String FieldContent
        {
            get { return this.m_fieldContent; }
            set { this.m_fieldContent = value; }
        } 
        
    }
}
