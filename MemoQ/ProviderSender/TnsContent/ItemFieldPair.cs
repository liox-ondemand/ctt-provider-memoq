﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClayTablet.SC.Translation
{
    
    [Serializable]
    public class ItemFieldPair
    {
        private String m_fieldName;
        private String m_fieldContent_Source;
        private String m_fieldContent_Target;

        public ItemFieldPair()
        {
        }

        public ItemFieldPair(String fieldName, String fieldContent_Source, String fieldContent_Target)
        {
            this.m_fieldName = fieldName;
            this.m_fieldContent_Source = fieldContent_Source;
            this.m_fieldContent_Target = fieldContent_Target;
        }

        public String FieldName
        {
            get { return this.m_fieldName; }
            set { this.m_fieldName = value; }
        }

        public String FieldContent_Source
        {
            get { return this.m_fieldContent_Source; }
            set { this.m_fieldContent_Source = value; }
        }

        public String FieldContent_Target
        {
            get { return this.m_fieldContent_Target; }
            set { this.m_fieldContent_Target = value; }
        }

    }
}
