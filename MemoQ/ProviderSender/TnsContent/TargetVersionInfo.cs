using System;
using System.Collections.Generic;
using System.Text;

namespace ClayTablet.SC.Translation
{
    [Serializable]
    public class TargetVersionInfo
    {
        protected String m_targetLanguage;
        protected int m_targetVersion;

        public TargetVersionInfo()
        {
             
        }

        public TargetVersionInfo(String targetLanguage, int targetVersion) 
        {
            m_targetLanguage = targetLanguage;
            m_targetVersion = targetVersion;
        }

        public String targetLanguage
        {
            get { return this.m_targetLanguage; }
            set { this.m_targetLanguage = value; }
        }

        public int targetVersion
        {
            get { return this.m_targetVersion; }
            set { this.m_targetVersion = value; }
        }
    }
}
