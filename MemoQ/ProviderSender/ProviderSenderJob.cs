using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using com.claytablet.model;
using com.claytablet.provider;
using com.claytablet.model.enm;
using com.claytablet.model.Event;
using com.claytablet.model.Event.platform;
using com.claytablet.model.Event.provider;
using com.claytablet.service.Event;
using com.claytablet.util;
using com.claytablet.queue;
using com.claytablet.queue.service;
using com.claytablet.queue.service.sqs;
using com.claytablet.storage;
using com.claytablet.storage.service;
using com.claytablet.storage.service.s3;
using ClayTablet.CT.Utility;
using ClayTablet.SC.Translation;
using Utility;
using System.IO;
using Utility.ServerProjectService;
using System.Linq;

namespace ClayTablet.MemoQ
{
    public class ProviderSenderJob
    {
        public void Process()
        { 

            SourceAccountProvider sap;
            TargetAccountProvider tap;

            QueueSubscriberService queueSubscriberService;
            QueuePublisherService queuePublisherService;
            StorageClientService storageClientService;

            StorageDirectoryProvider context;
            ProviderSender sender;

            /*
            * sample config setting
            * 
            * Check the app.config in the sample
            * for web applications,  config goes web.config file.
            * 
            * when config the three keys below, please use absolute path
            * Since CT 2.0 may be used in web applecations, absolute path (can be figured outside of web folder)
            * to keep account info and connecitonContent info more secure.
            * 
                <appSettings>
		            <add key="ClayTablet.Provider.SourceAccount" value="E:\ctt_Core2.0_NET\CT2.0_Samples\ProviderSample\accounts\source.xml" />
		            <add key="ClayTablet.Provider.TargetAccount" value="E:\ctt_Core2.0_NET\CT2.0_Samples\ProviderSample\accounts\target.xml" />
		            <add key="ClayTablet.Provider.ContextFolder" value="E:\ctt_Core2.0_NET\CT2.0_Samples\ProviderSample\data\" />
	            </appSettings>
            * 
            *  
            */

            CLogger.WriteLog(ELogLevel.DEBUG, "Sender processing jobs");

            var sourceAccountConfig = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SourceAccount"];
            String sourceAccountFile = null;
            if (sourceAccountConfig == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Source Account file [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Provider.SourceAccount] in AppSetting to point a source Account file and make sure system have read permission with the Account file.");
                return;
            }
            else
            {
                sourceAccountFile = PathUtil.getFullPath4RelatedPath(sourceAccountConfig.ToString());
            }

            String targetAccountFile = null;
            var targetAccountConfig = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.TargetAccount"];
            if (targetAccountConfig == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Target Account file [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Provider.TargetAccount] in AppSetting to point a target Account file and make sure system have read permission with the Account file.");
                return;
            }
            else
            {
                targetAccountFile = PathUtil.getFullPath4RelatedPath(targetAccountConfig.ToString());
            }

            String clientId = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ClientId"];

            context = ProviderAssetTaskTranslationMap.Instance;

            String sourceAccountKey = FileUtil.ReadStringFromFile(sourceAccountFile);

            //Initial a source Account,  SDK will look for the source account file which appsetting ("ClayTablet.Provider.SourceAccount") pointed.
            sap = new SourceAccountProvider(clientId, sourceAccountKey);

            String targetAccountKey = FileUtil.ReadStringFromFile(targetAccountFile);

            //Initial a target Account,  SDK will look for the source account file which appsetting ("ClayTablet.Provider.TargetAccount") pointed.         
            tap = new TargetAccountProvider(clientId, targetAccountKey);

            Account sourceAccount = sap.get();
            if (sourceAccount == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot initialize source account from " + sourceAccountFile);
                return;
            }

            Account targetAccount = tap.get();

            if (targetAccount == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot initialize target account from " + targetAccountFile);
                return;
            }

            storageClientService = new StorageClientServiceS3();
            storageClientService.setPublicKey(sourceAccount.getPublicKey());
            storageClientService.setPrivateKey(sourceAccount.getPrivateKey());
            storageClientService.setStorageBucket(sourceAccount.getStorageBucket());

            queuePublisherService = new QueuePublisherServiceSQS();
            queuePublisherService.setPublicKey(sourceAccount.getPublicKey());
            queuePublisherService.setPrivateKey(sourceAccount.getPrivateKey());
            queuePublisherService.setEndpoint(sourceAccount.getQueueEndpoint());

            queueSubscriberService = new QueueSubscriberServiceSQS();
            queueSubscriberService.setPublicKey(sourceAccount.getPublicKey());
            queueSubscriberService.setPrivateKey(sourceAccount.getPrivateKey());
            queueSubscriberService.setEndpoint(sourceAccount.getQueueEndpoint());


            //Initial a ProviderSender, may need to send Event back.
            sender = new ProviderSender(context, sap, tap, queuePublisherService, storageClientService);

            ProviderAssetTaskTranslationMap assetTaskTranslationMap = ProviderAssetTaskTranslationMap.Instance;
            CLogger.WriteLog(ELogLevel.INFO, "Polling TMS for asset task state changes.");

            List<ProviderJobMapping> checkMappingList = assetTaskTranslationMap.ListJobs();

            try
            {

                foreach (ProviderJobMapping jobMapping in checkMappingList)
                {
                    if (!jobMapping.getisTranslated())
                    {
                        String tmsProjectId = jobMapping.getTmsProjectGUID();

                        String tmsDocumentId = jobMapping.getTmsDocumentGUID();

                        if (tmsProjectId != null && tmsDocumentId != null)
                        {
                            // CLogger.WriteLog(ELogLevel.DEBUG, "Check Files in : " + tns_CheckFolder);
                            try
                            {
                                String downloadFilePath = string.Empty;

                                var translationDocuments = MemoQUtil.GetServerProjectService().ListProjectTranslationDocuments(new Guid(tmsProjectId));

                                var documentInfo = translationDocuments.First(x => x.DocumentGuid.Equals(new Guid(tmsDocumentId)));

                                if (documentInfo == null)
                                {
                                    CLogger.WriteLog(ELogLevel.ERROR, "Cannot find the document");
                                    continue;
                                }

                                //if (documentInfo.DocumentStatus == DocumentStatus.ProofreadingFinished) 
                                //project complete condition  )
                                if (documentInfo.DocumentStatus == DocumentStatus.TranslationFinished)
                                {
                                    try
                                    {
                                        // TODO: download file
                                        CLogger.WriteLog(ELogLevel.INFO, "before download file");

                                        string exportedFileName = string.Empty;
                                        string exportDir = context.getTargetDirectory();
                                        if (!Directory.Exists(exportDir))
                                        {
                                            Directory.CreateDirectory(exportDir);
                                        }

                                        IServerProjectService serverProjectService = MemoQUtil.GetServerProjectService();

                                        TranslationDocExportResultInfo resultInfo = serverProjectService.ExportTranslationDocument(
                                            new Guid(tmsProjectId), new Guid(tmsDocumentId));
                                        if (resultInfo.ResultStatus == ResultStatus.Error)
                                            throw new Exception("There was an error during the export," +
                                            " with message: " + resultInfo.MainMessage);
                                        // Download the result of the export of document A
                                        MemoQUtil.DownloadFile(resultInfo.FileGuid, out exportedFileName, exportDir);

                                        downloadFilePath = Path.Combine(exportDir, exportedFileName);
                                        // Delete file on server, we no longer need it
                                        MemoQUtil.GetFileManagerService().DeleteFile(resultInfo.FileGuid);

                                        CLogger.WriteLog(ELogLevel.INFO, "downloaded to: " + downloadFilePath);

                                        #region submit asset task
                                        if (jobMapping.ReceivedEventType == EventType.NewTranslation)
                                        {
                                            CLogger.WriteLog(ELogLevel.INFO, "Handle downloaded file as EventType.NewTranslation");

                                            if (!String.IsNullOrEmpty(downloadFilePath))
                                            {

                                                SubmitAssetTask submitAssetTask = new SubmitAssetTask();
                                                submitAssetTask.setEventId(IdGenerator.createId());

                                                //set original CT2 AssetTaskId
                                                submitAssetTask.setAssetTaskId(jobMapping.getAssetTaskId());
                                                submitAssetTask.setNativeState("Translated");
                                                submitAssetTask.setFileExt(jobMapping.getFileExt());
                                                sender.sendEvent(submitAssetTask, downloadFilePath);
                                                CLogger.WriteLog(ELogLevel.INFO, "Send out translated file: " + downloadFilePath);
                                                jobMapping.setisTranslated(true);
                                                assetTaskTranslationMap.Add(jobMapping);

                                                try
                                                {
                                                    if (File.Exists(downloadFilePath))
                                                    {
                                                        File.Delete(downloadFilePath);
                                                    }
                                                }
                                                catch (Exception exp)
                                                {
                                                    CLogger.WriteLog(ELogLevel.ERROR, "Cannot delete target file at " + downloadFilePath, exp);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (jobMapping.ReceivedEventType == EventType.TranslationFix)
                                            {
                                                CLogger.WriteLog(ELogLevel.INFO, "Handle downloaded file as EventType.TranslationFix");

                                                if (!String.IsNullOrEmpty(downloadFilePath))
                                                {
                                                    SubmitTranslationCorrectionAssetTask submitAssetTask = new SubmitTranslationCorrectionAssetTask();
                                                    submitAssetTask.setEventId(IdGenerator.createId());
                                                    //set original CT2 AssetTaskId
                                                    submitAssetTask.setAssetTaskId(jobMapping.getAssetTaskId());
                                                    submitAssetTask.setNativeState("Translated");
                                                    submitAssetTask.setFileExt(jobMapping.getFileExt());
                                                    sender.sendEvent(submitAssetTask, downloadFilePath);
                                                    CLogger.WriteLog(ELogLevel.INFO, "Sent out translated file: " + downloadFilePath);
                                                    try
                                                    {
                                                        if (File.Exists(downloadFilePath))
                                                        {
                                                            File.Delete(downloadFilePath);
                                                        }
                                                    }
                                                    catch (Exception exp)
                                                    {
                                                        CLogger.WriteLog(ELogLevel.ERROR, "Cannot delete target file at " + downloadFilePath, exp);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                    }
                                    catch (Exception e)
                                    {
                                        CLogger.WriteLog(ELogLevel.ERROR, "Failed to download and handle translated file: " + downloadFilePath);
                                        CLogger.WriteLog(ELogLevel.ERROR, "Error: " + e.Message);
                                    }

                                }


                            }
                            catch (Exception e) // should also catch tms exception here
                            {
                                CLogger.WriteLog(ELogLevel.ERROR, e.Message);
                            }
                        } //if have all info


                    } //each froup job
                }
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Unknown Error: \n" + e.Message);
            }
        
            CLogger.WriteLog(ELogLevel.DEBUG, "Sender done processing jobs");
        }
    }
}
