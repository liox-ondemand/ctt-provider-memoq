﻿using System;
using System.Collections.Generic;
using System.Text;
using Xstream.Core;

namespace ClayTablet.MemoQ
{
    public class LastEmailSending
    {
        private DateTime lastSentTime;

        public DateTime LastSentTime
        {
            get { return this.lastSentTime; }
            set { this.lastSentTime = value; }
        }

        public static void SaveLastSentTime(DateTime sentTime)
        {
            LastEmailSending curLastEmailSending = new LastEmailSending();
            curLastEmailSending.LastSentTime = sentTime;

            string filename = getFileName();
            if (System.IO.File.Exists(filename))
                System.IO.File.Delete(filename);

            string filexml = LastEmailSending.toXml(curLastEmailSending);

            com.claytablet.util.FileUtil.WriteStringToFile(filename, filexml);

        }

        public static DateTime GetLastSentTime()
        {

            string filename = getFileName();
            if (!System.IO.File.Exists(filename))
                LastEmailSending.SaveLastSentTime(DateTime.Now);

            string filexml = com.claytablet.util.FileUtil.ReadStringFromFile(filename);
            LastEmailSending curLastEmailSending = LastEmailSending.fromXml(filexml);

            return curLastEmailSending.LastSentTime;
        }

        private static string getFileName()
        {
            String DEFAULT_DATA_DIR = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ContextFolder"];
            if (DEFAULT_DATA_DIR.EndsWith("\\"))
                DEFAULT_DATA_DIR = DEFAULT_DATA_DIR + "SentEmails\\";
            else
                DEFAULT_DATA_DIR = DEFAULT_DATA_DIR + "\\SentEmails\\";

            if (!System.IO.Directory.Exists(DEFAULT_DATA_DIR))
                System.IO.Directory.CreateDirectory(DEFAULT_DATA_DIR);

            return DEFAULT_DATA_DIR + "SentTime.xml";

        }


        public static LastEmailSending fromXml(String xml)
        {

            // deserialize the account
            return (LastEmailSending)getXStream().FromXml(xml);
        }

        public static String toXml(LastEmailSending lastEmailSending)
        {

            // serilize the object to xml and return it
            return getXStream().ToXml(lastEmailSending);
        }

        private static XStream getXStream()
        {
            XStream xstream = new XStream();
            return xstream;
        }
    }
}
