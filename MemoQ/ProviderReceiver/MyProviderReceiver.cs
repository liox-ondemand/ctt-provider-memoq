using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using com.claytablet.model;
using com.claytablet.provider;
using com.claytablet.model.enm;
using com.claytablet.model.Event;
using com.claytablet.model.Event.platform;
using com.claytablet.model.Event.provider;
using com.claytablet.service.Event; 
using com.claytablet.util;
using com.claytablet.queue;
using com.claytablet.queue.model;
using com.claytablet.queue.service;
using com.claytablet.queue.service.sqs;
using com.claytablet.storage;
using com.claytablet.storage.service;
using com.claytablet.storage.service.s3;
using ClayTablet.CT.Utility;
using Utility;
using System.Net.Mail;
using System.Web;
using com.claytablet.model.Event.metadata;

using Utility.ServerProjectService;
using Utility.ResourceService;
using System.Globalization;

namespace ClayTablet.MemoQ
{
    public class MyProviderReceiver
    {
        // also connection config file
	    private  StorageDirectoryProvider context; 
	    private ProviderAssetTaskTranslationMap assetTaskTranslationMap;  

	    private StorageClientService storageClientService;    	
	    private com.claytablet.service.Event.ProviderSender sender;

        private const int connectToMemoQTimes = 5;
        private String tns_NotifyEmail = null;
        private bool useAssetName = false;
        private bool appendGuidToProjectName = false;

        private bool initSuccess = true;
        private const string LIOX_ENCODING = "Liox-v1|";
        private const string SITECORE35_ENCODING = "SiteCore-v35|";
        private const string CQ15_ENCODING = "CQ5-v15|";
        private const string CQ5GENERAL_ENCODING = "CQ5-general|";

        private string[] CQ15_SC_35_ENCODINGS = { LIOX_ENCODING, SITECORE35_ENCODING, CQ15_ENCODING };
        private string[] CQ5GENERAL_ENCODINGS = { CQ5GENERAL_ENCODING };

        public MyProviderReceiver(StorageDirectoryProvider context,
                                  StorageClientService storageClientService,
                                  ProviderSender sender)
        {
            // Pass a StorageDirectoryProvider, you need it to retrieve the path where 
            // the translated files will be saved
            this.context = context;

            //pass a StorageClientService, you need it to download file for translation
            this.storageClientService = storageClientService;

            /*ProviderJobMapping is a helper class to help mapping the CT 2.0 Info to your TMS info
             *ProviderAssetTaskTranslationMap is a helper class to handle saving/deleting/searching
             *the ProviderJobMapping objects
             * 
             *You can save the CT 2.0 Info to your TMS system if it supports
             * then you don't need the ProviderJobMapping and ProviderAssetTaskTranslationMap classes
             */
            this.assetTaskTranslationMap = ProviderAssetTaskTranslationMap.Instance;
            
            //Pass a ProducerSender, so you can send Events and Files out if needed
            //Check Sample-Sender for how to send Events and Files.
            this.sender = sender;

            bool needEmailNotification = false;
            if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.Required"] != null)
            {
                string needEmailNotification_setting = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.Required"].ToString().ToLower();

                if (needEmailNotification_setting.Equals("true"))
                    needEmailNotification = true;
                else
                    needEmailNotification = false;
            }

            if (needEmailNotification)
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.To.EmailAddresses"] == null)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Can't find To Emails config [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Notification.To.EmailAddresses] in AppSetting .");
                    return;
                }
                else
                {
                    tns_NotifyEmail = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.To.EmailAddresses"].ToString();
                }
            }
            string appendGuidToProjectNameValue = System.Configuration.ConfigurationManager.AppSettings["MemoQ_AppendGuidToProjectName"];
            if (appendGuidToProjectNameValue != null)
            {
                try
                {
                    appendGuidToProjectName = bool.Parse(appendGuidToProjectNameValue);
                } catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Invalid confiugration for 'MemoQ_AppendGuidToProjectName': " + appendGuidToProjectName, e);
                    appendGuidToProjectName = false;
                }
            }
        }


        public HandleResult ReceiveEvent(IEvent cttEvent)
        {

            if (cttEvent is ApprovedAssetTask)
                return receiveEvent((ApprovedAssetTask)cttEvent);
            else if (cttEvent is CanceledAssetTask)
                return receiveEvent((CanceledAssetTask)cttEvent);
            else if (cttEvent is CanceledSupportAsset)
                return receiveEvent((CanceledSupportAsset)cttEvent);
            else if (cttEvent is ProcessingError)
                return receiveEvent((ProcessingError)cttEvent);
            else if (cttEvent is RejectedAssetTask)
                return receiveEvent((RejectedAssetTask)cttEvent);
            else if (cttEvent is StartAssetTask)
                return receiveEvent((StartAssetTask)cttEvent);
            else if (cttEvent is StartNeedAssetTaskUpdate)
                return receiveEvent((StartNeedAssetTaskUpdate)cttEvent);
            else if (cttEvent is StartSupportAsset)
                return receiveEvent((StartSupportAsset)cttEvent);
            else if (cttEvent is StartUpdateTMAsset)
                return receiveEvent((StartUpdateTMAsset)cttEvent);
            else if (cttEvent is StartNeedTranslationCorrectionAssetTask)
                return receiveEvent((StartNeedTranslationCorrectionAssetTask)cttEvent);
            else if (cttEvent is AcknowledgedAssetTaskTranslation)
                return receiveEvent((AcknowledgedAssetTaskTranslation)cttEvent);
            else
            {
                HandleResult handleResult = new HandleResult();
                handleResult.Success = false;
                handleResult.CanDeleteMessage = true;
                handleResult.ErrorMessage = "Can't identify the Event Type.";

                return handleResult;
            }


        }

        private HandleResult receiveEvent(AcknowledgedAssetTaskTranslation ev)
        {
            CLogger.WriteLog(ELogLevel.INFO, 
                "Received AcknowledgedAssetTaskTranslation for asset task: " + ev.getName()  +
				" (" + ev.getAssetTaskId() + ") in project " + ev.getProjectName() + " (" + 
				ev.getProjectId() + ")");

            try
            {
                storageClientService.deleteAssetTaskVersions(ev.getAssetTaskId());
            }
            catch (Exception e) 
            {
                CLogger.WriteLog(ELogLevel.DEBUG, "Cannot delete asset task from S3", e);
            }

            return new SuccessHandleResult();
        }

        private HandleResult receiveEvent(StartNeedAssetTaskUpdate curEvent)
        {

            HandleResult handleResult = new HandleResult();
            //  call TMS to translate the AssetTask
            try
            { 
                string org_targetFilePath = context.getTargetDirectory() + curEvent.getAssetTaskId() + ".xml";

                // send AcceptAssetTask event out to notify CT 2.0 platform 
                AcceptNeedAssetTaskUpdate acceptEvent = new AcceptNeedAssetTaskUpdate();
                acceptEvent.setAssetTaskId(curEvent.getAssetTaskId());
                acceptEvent.setEventId(IdGenerator.createId());

                //call ProviderSender to sent event
                sender.sendEvent(acceptEvent);


                //event handling is done, so Message can be removed from SQS queue.
                return new SuccessHandleResult();
            }
            catch (Exception exp)  // should catch TMS exception here.
            {
                handleResult.CanDeleteMessage = false;
                handleResult.Success = false;
                handleResult.ErrorMessage = "[StartNeedAssetTaskUpdate] Error.";
                handleResult.Message = exp.Message;

            }

            return handleResult;
        }

        private HandleResult receiveEvent(ApprovedAssetTask ev) 
        {
            try
            {
                storageClientService.deleteAssetTaskVersions(ev.getAssetTaskId());
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.DEBUG, "Cannot delete asset task from S3", e);
            }

            //job is down, remove this job from Mapping
            assetTaskTranslationMap.RemoveArchive(ev.getAssetTaskId());

            ProviderJobMapping jobMapping = assetTaskTranslationMap.findJobByAssetTaskId(ev.getAssetTaskId());
            if (jobMapping != null)
            {
                assetTaskTranslationMap.Remove(jobMapping);
                CLogger.WriteLog(ELogLevel.INFO, "[ApprovedAssetTask] Removed job mapping " 
                    + jobMapping.getLocalFileGUID() +" for asset task: " + ev.getAssetTaskId());
            }
            else
            {
                CLogger.WriteLog(ELogLevel.DEBUG,
                    "[ApprovedAssetTask] Cannot found job mapping for asset task: " + ev.getAssetTaskId());
            }
            return new SuccessHandleResult();
	    }


        private HandleResult receiveEvent(CanceledAssetTask curEvent)
        {
            HandleResult handleResult = new HandleResult(); 
		    try {
    			
			    //The the info from JobMapping

                ProviderJobMapping mapJob = assetTaskTranslationMap.findJobByAssetTaskId(curEvent.getAssetTaskId());
    			 
			    //Find the providerFileId etc info from assetTaskMap
    			
			    //call TMS to cancel the Job
			    if ( mapJob != null  )
			    {
    				
				    String tmsProjectId = mapJob.getTmsProjectGUID();
				    String tmsDocumentId = mapJob.getTmsDocumentGUID(); 
    				
				    String tms_Url = mapJob.getServerUrl(); ;
				    String tms_LoginName = mapJob.getLoginName() ;
				    String tms_LoginPassword = mapJob.getLoginPassword(); 
    				
				    //TODO, connect to TMS to cancel ....	

                    CLogger.WriteLog(ELogLevel.INFO, "[CanceledAssetTask] Removed job mapping "
                        + mapJob.getLocalFileGUID() + " for asset task: " + curEvent.getAssetTaskId());

                    handleResult.CanDeleteMessage = true;
                    handleResult.Success = true;    				 
			    }

                try
                {

                    storageClientService.deleteAssetTaskVersions(curEvent.getAssetTaskId());
                }
                catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.DEBUG, "Failed to remove file from S3 for asset task: " + curEvent.getAssetTaskId(), e);
                }


		    } catch (Exception e) {

                handleResult.CanDeleteMessage = false;
                handleResult.Success = false;
                handleResult.ErrorMessage = "[CanceledAssetTask] Error.";
                handleResult.Message = e.Message;
		    }

            return handleResult;
	    }


        private HandleResult receiveEvent(CanceledSupportAsset curEvent)
        {
            return new SuccessHandleResult();
	    }


        private HandleResult receiveEvent(ProcessingError curEvent)
        {
            CLogger.WriteLog(ELogLevel.INFO, "Get a ProcessingError:" );
            CLogger.WriteLog(ELogLevel.INFO, "------------ Error Message --------------");
            CLogger.WriteLog(ELogLevel.INFO, curEvent.getErrorMessage() );
            CLogger.WriteLog(ELogLevel.INFO, "------------ Error Details --------------");
            CLogger.WriteLog(ELogLevel.INFO, curEvent.getErrorDetails() + "\n\n" );

            return new SuccessHandleResult();
        }


        private HandleResult receiveEvent(RejectedAssetTask curEvent)
        {
            ProviderJobMapping jobMapping = assetTaskTranslationMap.findJobByAssetTaskId(curEvent.getAssetTaskId());
            string assetTaskDetail;

            if (jobMapping != null)
            {
                assetTaskDetail = "Asset task id: " + curEvent.getAssetTaskId() + "    ";
                assetTaskDetail += "\nFile name: " + jobMapping.getFileName() + "    ";
                assetTaskDetail += "\nDrop folder: " + jobMapping.getDropfolder() + "    ";
                assetTaskDetail += "\nSource language: " + jobMapping.getSourceTmsLanguageCode() + "    ";
                assetTaskDetail += "\nTarget language: " + jobMapping.getTargetTmsLanguageCode() + "    ";
            }
            else
            {
                assetTaskDetail = "Asset task id: " + curEvent.getAssetTaskId();
                assetTaskDetail += "File extension: " + curEvent.getFileExt();
                assetTaskDetail += "\n(Sorry we cannot find out more information about the asset task)";
            }
            
            if (!String.IsNullOrEmpty(tns_NotifyEmail))
            {
                // send email notification
                string msg = "";
                msg += "Hello,\n";
                msg += "A translation you sent back to client via Clay-Tablet has been rejected with the following note:\n";
                msg += curEvent.getReviewNote() + "\n\n";
                msg += assetTaskDetail;

                msg += "\nThis is an automatic notification email.\n";
                msg += "If you feel you are receiving this email in error, please contact the system administrator at: admin@clay-tablet.com \n\n\n";
                msg += "==========================\n";
                msg += "Generated by:\n";
                msg += "Rosetta | Trans-port.\n\n";

                msg += "Clay Tablet Technologies\n";
                msg += "www.Clay-Tablet.com\n";
                msg += "Simply Global.\n";
                msg += "==========================\n\n";


                NotifyEmail newNotifyEmail = new NotifyEmail();
                newNotifyEmail.EmailBody = msg;
                newNotifyEmail.EmailAddress = tns_NotifyEmail;
                newNotifyEmail.IsPriority = true;

                // Attach a processing error event to the email, if the email fails to send, the event 
                // will be sent back to producer
                AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(),
                        "RejectedAssetTask processing",
                        "Cannot notify LSP about RejectedAssetTask - fail to send email notification",
                        curEvent.getReviewNote(), false);
                newNotifyEmail.EventToSendWhenFail = error;

                newNotifyEmail.Save();
            }
            else
            {
                // log the event
                string msg = "Translation for asset task " + curEvent.getAssetTaskId() +
                    " rejected for the following reasons, but we cannot notify LSP as email notification is not set up";
                CLogger.WriteLog(ELogLevel.WARN, msg);
                CLogger.WriteLog(ELogLevel.WARN, curEvent.getReviewNote());

                AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(), 
                        "RejectedAssetTask processing", 
                        "Cannot notify LSP about RejectedAssetTask - email notification not configured", 
                        msg + ": " + curEvent.getReviewNote(), false);
                sender.sendEvent(error);
            }

            return new SuccessHandleResult();
        }


        private HandleResult receiveEvent(StartAssetTask curEvent)
        {

    		HandleResult handleResult = new HandleResult();
            
            //String projectName = null;
            
		    //  call TMS to translate the AssetTask
            if ( initSuccess )
            {
                try
                {
                    string projectId = string.Empty;
                    int assetTaskCount = 1;
                    string description = string.Empty;
                    IJobMetadata metadata = curEvent.getJobMetadata();
                    if ( metadata != null )
                    {
                        if (metadata.getDescription() != null)
                            description = metadata.getDescription();
                        projectId = metadata.getJobId();
                        assetTaskCount = metadata.getAssetTaskCount();
                    }
                    else
                    {
                        getInfoFromTagsOrDesc(curEvent.getTags(), curEvent.getDescription(), ref assetTaskCount, ref projectId);
                    }
                    CLogger.WriteLog(ELogLevel.INFO, string.Format("projectId is {0}", projectId));
                    CLogger.WriteLog(ELogLevel.INFO, string.Format("Task count of project {0} is {1}", projectId, assetTaskCount));

                    if ( string.IsNullOrEmpty(projectId) )
                    {
                        string errorMessage = "Cannot get project id from metadata or project description.";
                        CLogger.WriteLog(ELogLevel.ERROR, errorMessage);
                        handleResult.CanDeleteMessage = false;
                        handleResult.ErrorMessage = errorMessage;
                        handleResult.Success = false;
                        handleResult.Message = "Get project id from metadata or project description";

                        AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(),
                                handleResult.Message, errorMessage, "", false);
                        sender.sendEvent(error);
                        return handleResult;
                    }

                    // Download the latest asset task revision file  
                    String downloadFilePath = storageClientService
                            .downloadLatestAssetTaskVersion(curEvent.getAssetTaskId(), context.getSourceDirectory());

                    downloadFilePath = rectifyFilename(downloadFilePath, curEvent.getAssetTaskId(), curEvent.getName());

                    #region create tms project

                    // Convert CT 2.0 language code to tms language code
                    String tms_SoureLanguageCode = MemoQUtil.ConvertCTTLanguageCodeToMemoQLanguageCode(
                        curEvent.getSourceLanguageCode());
                    String tms_TargetLanguageCode = MemoQUtil.ConvertCTTLanguageCodeToMemoQLanguageCode(
                        curEvent.getTargetLanguageCode());
                    #region clarify whether the corresponding language code exsits
                    if ( string.IsNullOrEmpty(tms_SoureLanguageCode) )
                    {
                        string errorMessage = string.Format("Cannot find corresponding language code for ctt language code: {0}. ", curEvent.getSourceLanguageCode());
                        CLogger.WriteLog(ELogLevel.ERROR, errorMessage);
                        handleResult.CanDeleteMessage = false;
                        handleResult.ErrorMessage = errorMessage;
                        handleResult.Success = false;
                        handleResult.Message = "Convert ctt language code to MemoQ language code";

                        AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(),
                                handleResult.Message, errorMessage, "", false);
                        sender.sendEvent(error);
                        return handleResult;
                    }
                    if ( string.IsNullOrEmpty(tms_TargetLanguageCode) )
                    {
                        string errorMessage = string.Format("Cannot find corresponding language code for ctt language code: {0}. ", curEvent.getTargetLanguageCode());
                        CLogger.WriteLog(ELogLevel.ERROR, errorMessage);
                        handleResult.CanDeleteMessage = false;
                        handleResult.ErrorMessage = errorMessage;
                        handleResult.Success = false;
                        handleResult.Message = "Convert ctt language code to MemoQ language code";

                        AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(),
                                handleResult.Message, errorMessage, "", false);
                        sender.sendEvent(error);
                        return handleResult;
                    }
                    #endregion
                    // TODO: 
                    //check if same project exists 
                    //The connector should be able to unpack a job from producer, creating multiple MemoQ projects as needed (one per target language). 
                    String tmsProjectGUID = assetTaskTranslationMap.SearchSameTmsProjectGUID(projectId, curEvent.getTargetLanguageCode());
                    DateTime dueDate = curEvent.getProjectDeadline();

                    if (metadata != null && !String.IsNullOrEmpty(metadata.getDueDate()))
                    {
                        try
                        {
                            dueDate = DateTime.Parse(metadata.getDueDate(), CultureInfo.CreateSpecificCulture("en-US"));
                        }
                        catch (Exception e)
                        {
                            CLogger.WriteLog(ELogLevel.WARN, "Cannot convert job due date: " + metadata.getDueDate());
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                        }
                    }
                    if (dueDate==null || dueDate < DateTime.Now)
                    {
                        dueDate = DateTime.Now.AddDays(2);
                    }

                    String tmsProjectName = metadata==null? "SP_" + Guid.NewGuid().ToString() : 
                        normalizeProjectName(metadata.getJobName(), appendGuidToProjectName ? "-" + metadata.getJobId() : "");

                    String tmsdocumentGUID = null;
                    String filename = Path.GetFileName(downloadFilePath);
                    IServerProjectService serverProjectService = MemoQUtil.GetServerProjectService();

                    #region get user
                    Guid userGuid = Guid.Empty;
                    global::Utility.SecurityService.UserInfo user = null;
                    global::Utility.SecurityService.UserInfo[] userInfos = MemoQUtil.ListUsers();
                    if ( userInfos != null && userInfos.Length > 0 )
                    {
                        user = userInfos[0];
                        userGuid = userInfos[0].UserGuid;
                    }
                    #endregion

                    if ( tmsProjectGUID == null )
                    {
                        try
                        {
                            CLogger.WriteLog(ELogLevel.INFO, "Project creator: " + userGuid.ToString());
                            CLogger.WriteLog(ELogLevel.INFO, "Create server project...");
                            CLogger.WriteLog(ELogLevel.DEBUG, "Project dueDate: " + dueDate);
                            string projectTemplateGuid = System.Configuration.ConfigurationManager.AppSettings["MemoQ_ProjectTemplateGuid"];

                            if (string.IsNullOrEmpty(projectTemplateGuid))
                            {
                                CLogger.WriteLog(ELogLevel.INFO, "Creating live docs project: " + tmsProjectName);
                                tmsProjectGUID = MemoQUtil.CreateLiveDocsProject(tmsProjectName, tms_SoureLanguageCode, new string[] { tms_TargetLanguageCode },
                                    false, userGuid, description, dueDate, serverProjectService).ToString();
                                CLogger.WriteLog(ELogLevel.INFO, "Created project: " + tmsProjectName + "(" + tmsProjectGUID + ")");

                                CLogger.WriteLog(ELogLevel.INFO, "Create translation memory");
                                string projectName = curEvent.getProjectName().ToString();
                                if (Encoding.UTF8.GetByteCount(projectName) > 50)
                                {
                                    projectName = truncateToByteLength(projectName, 50);
                                }

                                Guid tmGuid = MemoQUtil.CreateAndPublishTM(projectName + "_TM_" + Guid.NewGuid().ToString(),
                                    string.Format("{0} to {1}", tms_SoureLanguageCode, tms_TargetLanguageCode),
                                    tms_SoureLanguageCode, tms_TargetLanguageCode);
                                CLogger.WriteLog(ELogLevel.INFO, "Prepare TM assignments for target language");
                                ServerProjectTMAssignmentsForTargetLang tmAssignments
                                        = new ServerProjectTMAssignmentsForTargetLang();
                                tmAssignments.TargetLangCode = tms_TargetLanguageCode;
                                tmAssignments.TMGuids = new Guid[] { tmGuid };
                                tmAssignments.PrimaryTMGuid = tmGuid;
                                // Apply TM assignmenst
                                serverProjectService.SetProjectTMs2(new Guid(tmsProjectGUID),
                                    new ServerProjectTMAssignmentsForTargetLang[] { tmAssignments });


                                CLogger.WriteLog(ELogLevel.INFO, "Create term base");
                                Guid tbGuid = MemoQUtil.CreateAndPublishTB(projectName + "_TB_" + Guid.NewGuid().ToString(),
                                    string.Format("{0} to {1}", tms_SoureLanguageCode, tms_TargetLanguageCode),
                                    new string[] { tms_SoureLanguageCode, tms_TargetLanguageCode });
                                // Assign the TB to the project
                                serverProjectService.SetProjectTBs(new Guid(tmsProjectGUID), new Guid[] { tbGuid }, tbGuid);

                                CLogger.WriteLog(ELogLevel.INFO, "Assign user: " + user.UserName);
                                ServerProjectUserInfo serverProjectUser = new ServerProjectUserInfo();
                                serverProjectUser.UserGuid = user.UserGuid;
                                serverProjectUser.ProjectRoles = new ServerProjectRoles();
                                serverProjectUser.ProjectRoles.ProjectManager = true;
                                serverProjectUser.ProjectRoles.Terminologist = true;
                                serverProjectService.SetProjectUsers(new Guid(tmsProjectGUID), new ServerProjectUserInfo[] { serverProjectUser });

                                //CLogger.WriteLog(ELogLevel.INFO, "Assign some light resources to the project");
                                //IResourceService resourceService = MemoQUtil.GetResourceService();
                            }
                            else
                            {   // create template-based project 
                                CLogger.WriteLog(ELogLevel.INFO, "Creating project: " + tmsProjectName + " based on template " + projectTemplateGuid);
                                tmsProjectGUID = MemoQUtil.CreateTemplateBasedProject(tmsProjectName, new Guid(projectTemplateGuid), tms_SoureLanguageCode, 
                                    new string[] { tms_TargetLanguageCode }, userGuid, description, dueDate, serverProjectService).ToString();
                                CLogger.WriteLog(ELogLevel.INFO, "Created project: " + tmsProjectName + "(" + tmsProjectGUID + ")");

                                CLogger.WriteLog(ELogLevel.INFO, "Assign user: " + user.UserName);
                                ServerProjectUserInfo serverProjectUser = new ServerProjectUserInfo();
                                serverProjectUser.UserGuid = user.UserGuid;
                                serverProjectUser.ProjectRoles = new ServerProjectRoles();
                                serverProjectUser.ProjectRoles.ProjectManager = true;
                                serverProjectUser.ProjectRoles.Terminologist = true;
                                serverProjectService.SetProjectUsers(new Guid(tmsProjectGUID), new ServerProjectUserInfo[] { serverProjectUser });
                            }

                        }
                        catch ( Exception e )
                        {
                            CLogger.WriteLog(ELogLevel.ERROR, "Cannot create server project " + tmsProjectName, e);
                            handleResult.CanDeleteMessage = false;
                            handleResult.Success = false;
                            handleResult.ErrorMessage = "[StartAssetTask] Processing Error.";
                            handleResult.Message = e.Message + "\nStackTrace:" + e.StackTrace; ;

                            AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(),
                                    handleResult.ErrorMessage, handleResult.Message, e.StackTrace, false);
                            sender.sendEvent(error);
                            return handleResult;
                        }
                    }
                    else
                    {
                        CLogger.WriteLog(ELogLevel.INFO, string.Format("The tms project {0} has already existed", tmsProjectGUID));
                    }
                    #endregion
                    #region upload and import document

                    try
                    {
                        // upload
                        Guid fileGuid = MemoQUtil.UploadFile(downloadFilePath);

                        // import
                        TranslationDocImportResultInfo trResultInfo = serverProjectService.ImportTranslationDocument(
                            new Guid(tmsProjectGUID), fileGuid, null, null);
                        // Delete file on server, we no longer need it
                        MemoQUtil.GetFileManagerService().DeleteFile(fileGuid);
                        if ( trResultInfo.ResultStatus == ResultStatus.Error )
                        {
                            // Handle error, we have the error message in
                            // trResultInfo.MainMessageField
                            // and trResultInfo.MainMessageField

                            throw new Exception("Document import failed: " +
                                trResultInfo.MainMessage);
                        }
                        else
                        {
                            tmsdocumentGUID = trResultInfo.DocumentGuids[0].ToString();

                            // First prepare assigmnent
                            ServerProjectTranslationDocumentUserAssignments translationDocUserAssign =
                                new ServerProjectTranslationDocumentUserAssignments();
                            translationDocUserAssign.DocumentGuid = new Guid(tmsdocumentGUID);
                            translationDocUserAssign.UserRoleAssignments =
                                new TranslationDocumentUserRoleAssignment[1];

                            // Assign admin to the as Translator document assignment role
                            translationDocUserAssign.UserRoleAssignments[0] =
                                new TranslationDocumentUserRoleAssignment();
                            translationDocUserAssign.UserRoleAssignments[0].UserGuid = userGuid;
                            translationDocUserAssign.UserRoleAssignments[0].DocumentAssignmentRole = 0;
                            translationDocUserAssign.UserRoleAssignments[0].DeadLine = dueDate;
                            // Now save the assignment we have prepared
                            ServerProjectTranslationDocumentUserAssignments[] documentUserAssignments =
                                new ServerProjectTranslationDocumentUserAssignments[] { translationDocUserAssign };
                            serverProjectService.SetProjectTranslationDocumentUserAssignments(new Guid(tmsProjectGUID),
                                documentUserAssignments);
                        }
                    }
                    catch ( Exception e )
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, string.Format("Cannot upload or import file to project: {0}", tmsProjectGUID), e);
                        handleResult.CanDeleteMessage = false;
                        handleResult.Success = false;
                        handleResult.ErrorMessage = "[StartAssetTask] Processing Error.";
                        handleResult.Message = e.Message + "\nStackTrace:" + e.StackTrace; ;

                        AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(),
                                handleResult.ErrorMessage, handleResult.Message, e.StackTrace, false);
                        sender.sendEvent(error);
                        return handleResult;
                    }

                    #endregion

                    // Save info to ProviderJobMapping  
                    ProviderJobMapping jobMapping = new ProviderJobMapping();

                    jobMapping.setAssetTaskId(curEvent.getAssetTaskId());
                    jobMapping.setAssetId(curEvent.getAssetId());
                    jobMapping.setProjectId(projectId);
                    jobMapping.ReceivedEventType = EventType.NewTranslation;

                    jobMapping.setTmsDocumentGUID(tmsdocumentGUID);
                    jobMapping.setLocalFileGUID(IdGenerator.createId());
                    jobMapping.setTmsProjectGUID(tmsProjectGUID);

                    jobMapping.setFileExt(curEvent.getFileExt());
                    jobMapping.setFileName(filename);
                    //jobMapping.setLoginName(GCMSUtil.GCMSParam.GetLoginName());
                    //jobMapping.setLoginPassword(GCMSUtil.GCMSParam.GetLoginPassword());
                    //jobMapping.setServerUrl(GCMSUtil.GCMSParam.GetServiceUrlBase());

                    jobMapping.setSourceCttLanguageCode(curEvent.getSourceLanguageCode());
                    jobMapping.setSourceTmsLanguageCode(tms_SoureLanguageCode);
                    jobMapping.setTargetCttLanguageCode(curEvent.getTargetLanguageCode());
                    jobMapping.setTargetTmsLanguageCode(tms_TargetLanguageCode);

                    //Call helper class to handle saving info
                    assetTaskTranslationMap.Add(jobMapping);

                    #region Pre-translate and distribute all docs for all target languages
                    try
                    {
                        List<ProviderJobMapping> cttProjectIdJobMappingList = assetTaskTranslationMap.SearchTasksByCttProjectId(projectId);
                        if ( cttProjectIdJobMappingList.Count == assetTaskCount )
                        {
                            // all files are downloaded in a ctt project. one ctt project --- multi memoq project
                            List<string> tmsProjectGUIDList = assetTaskTranslationMap
                                .SearchTmsProjectGUIDListByCttProjectId(projectId, cttProjectIdJobMappingList);
                            CLogger.WriteLog(ELogLevel.INFO, string.Format("one ctt project corresponding {0} memoq project", tmsProjectGUIDList.Count));

                            foreach ( string tmpTmsProjectGUID in tmsProjectGUIDList )
                            {
                                // Please note that our TMs are empty, so no mathes are found.
                                PretranslateOptions pretranslateOptions = new PretranslateOptions();
                                pretranslateOptions.GoodMatchRate = 80;
                                pretranslateOptions.OnlyUnambiguousMatches = false;
                                pretranslateOptions.PretranslateLookupBehavior =
                                    PretranslateLookupBehavior.AnyMatch;
                                pretranslateOptions.UseMT = false;
                                serverProjectService.PretranslateProject(new Guid(tmpTmsProjectGUID), null, pretranslateOptions);
                                serverProjectService.DistributeProject(new Guid(tmpTmsProjectGUID));
                            }
                        }
                    }
                    catch ( Exception e )
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, string.Format("Cannot finalize tms project {0}. ", tmsProjectGUID), e);
                        handleResult.CanDeleteMessage = false;
                        handleResult.ErrorMessage = e.Message;
                        handleResult.Success = false;
                        handleResult.Message = string.Format("Finalize tms project [{0}] error!", tmsProjectGUID);

                        return handleResult;
                    }
                    #endregion

                    // send AcceptAssetTask event out to notify Clay Tablet platform 
                    AcceptAssetTask acceptEvent = new AcceptAssetTask();
                    acceptEvent.setAssetTaskId(curEvent.getAssetTaskId());
                    acceptEvent.setAssetTaskNativeId(filename);
                    acceptEvent.setEventId(IdGenerator.createId());

                    //call ProviderSender to sent event
                    sender.sendEvent(acceptEvent);

                    //event handling is done, so Message can be removed from SQS queue.
                    handleResult.CanDeleteMessage = true;
                    handleResult.Success = true;
                    //return handleResult; 

                    try
                    {
                        storageClientService.deleteAssetTaskVersions(curEvent.getAssetTaskId());
                    }
                    catch ( Exception e )
                    {
                        CLogger.WriteLog(ELogLevel.WARN, "Cannot delete asset task version for " + downloadFilePath, e);
                    }


                }
                catch ( Exception e )
                {
                    handleResult.CanDeleteMessage = false;
                    handleResult.Success = false;
                    handleResult.ErrorMessage = "[StartAssetTask] Processing Error.";
                    handleResult.Message = e.Message + "\nStackTrace:" + e.StackTrace; ;

                    AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(),
                            handleResult.ErrorMessage, handleResult.Message, e.StackTrace, false);
                    sender.sendEvent(error);
                }
            }
            else
            {
                handleResult.CanDeleteMessage = false;
                handleResult.Success = false;
                handleResult.ErrorMessage = "[StartAssetTask] Configuration Error.";
                handleResult.Message = "MemoQ server connector has not been properly configured.";

                CLogger.WriteLog(ELogLevel.ERROR, handleResult.ErrorMessage + ": " + handleResult.Message);

                AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(),
                        handleResult.ErrorMessage, handleResult.Message, handleResult.Message, false);
                sender.sendEvent(error);
            }
            return handleResult;
	    }

        private string normalizeProjectName(string name, string suffix)
        {
            StringBuilder builder = new StringBuilder();

            foreach (char c in name)
            {
                if (char.IsLetterOrDigit(c) || c == '-' || c=='_')
                {
                    builder.Append(c);
                }
                else
                {
                    builder.Append('_');
                }
                if (builder.Length + suffix.Length>=254)
                {
                    break;
                }
            }
            builder.Append(suffix);
            return builder.ToString();
        }

        private HandleResult receiveEvent(StartNeedTranslationCorrectionAssetTask curEvent)
        {

            HandleResult handleResult = new HandleResult();
            //  call TMS to translate the StartNeedTranslationCorrectionAssetTask
            
            if (initSuccess)
            {

                try
                {
                    CLogger.WriteLog(ELogLevel.INFO, "Received StartNeedTranslationCorrectionAssetTask for asset task: "
                        + curEvent.getName() + " (" + curEvent.getAssetTaskId() + ") in project " 
                        + curEvent.getProjectName() + " (" + curEvent.getProjectId() + ")");
                    try
                    {
                        storageClientService.deleteAssetTaskVersions(curEvent.getAssetTaskId());
                    }
                    catch (Exception e)
                    {
                        CLogger.WriteLog(ELogLevel.WARN, "Cannot delete asset task version for " + curEvent.getAssetTaskId(), e);
                    }
                }
                catch (Exception e)
                {

                    handleResult.CanDeleteMessage = false;
                    handleResult.Success = false;
                    handleResult.ErrorMessage = "[StartNeedTranslationCorrectionAssetTask] Error.";
                    handleResult.Message = e.Message;

                    AssetTaskProviderError error = new AssetTaskProviderError(curEvent.getAssetTaskId(),
                            handleResult.ErrorMessage, e.Message, e.StackTrace, false);
                    sender.sendEvent(error);
                }
            }
            else
            {
                handleResult.CanDeleteMessage = false;
                handleResult.Success = false;
                handleResult.ErrorMessage = "[StartNeedTranslationCorrectionAssetTask] Error.";
                handleResult.Message = "MemoQ connector has not been properly configured.";
            }
            
            return handleResult;
        }

        private HandleResult receiveEvent(StartUpdateTMAsset curEvent)
        {

            HandleResult handleResult = new HandleResult();
            //  call TMS to translate the AssetTask
            try
            {
                if (initSuccess)
                {

                    try
                    {

                        CLogger.WriteLog(ELogLevel.INFO, "Start to downloaded UpdateTM file");
                        String downloadFilePath = storageClientService
                                .downloadUpdateTMAsset(curEvent.getUpdateTMAssetId(), curEvent.getFileExt(), context.getSourceDirectory());

                        //TODO, you maybe need to convert CT 2.0 Language Code to TMS language code.

                        //check if same project exists
                        String tmsProjectGUID = com.claytablet.util.IdGenerator.createId();

                        String tmpdocumentGUID = tmsProjectGUID;
                        String fileObjectName = System.IO.Path.GetFileName(downloadFilePath);
                        //TODO,  Add a project document to TMS, put => tmpdocumentGUID

                        //event handling is done, so Message can be removed from SQS queue.
                        handleResult.CanDeleteMessage = true;
                        handleResult.Success = true;

                        try
                        {
                            storageClientService.deleteUpdateTMAsset(curEvent.getUpdateTMAssetId(), curEvent.getFileExt());
                        }
                        catch (Exception e)
                        {
                            CLogger.WriteLog(ELogLevel.WARN, "Cannot delete asset task version for " + downloadFilePath, e);
                        }


                    }
                    catch (Exception exp)  // should catch TMS exception here.
                    {
                        handleResult.CanDeleteMessage = false;
                        handleResult.Success = false;
                        handleResult.ErrorMessage = "[StartUpdateTMAsset] Error.";
                        handleResult.Message = exp.Message;

                    }
                }
                else
                {
                    handleResult.CanDeleteMessage = false;
                    handleResult.Success = false;
                    handleResult.ErrorMessage = "[StartUpdateTMAsset] Error.";
                    handleResult.Message = "MemoQ connector has not been properly configured.";
                }

            }
            catch (Exception e)
            {
                handleResult.CanDeleteMessage = false;
                handleResult.Success = false;
                handleResult.ErrorMessage = "[StartUpdateTMAsset] Error.";
                handleResult.Message = e.Message;
            }
           
            return handleResult;
        }

        private string rectifyFilename(string downloadFilePath, string assetTaskId, string assetName)
        {
            if (!useAssetName)
            {
                return downloadFilePath;
            }
            else
            {
                string directoryName = Path.GetDirectoryName(downloadFilePath);
                string ext = Path.GetExtension(downloadFilePath);
                string filename = ToFileName(assetName, 80 - assetTaskId.Length - ext.Length) + '_' + assetTaskId + ext;
                string fullName = Path.Combine(directoryName, filename);
                try
                {
                    File.Move(downloadFilePath, fullName);
                    return fullName;
                }
                catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.WARN, 
                        "Cannot move " + downloadFilePath + " to " + fullName + ", leave it where it is", e);
                    return downloadFilePath;
                }
            }
        }

        private string ToFileName(string assetName, int length)
        {
            String results = assetName.Trim();
            results = results.Replace("/", "_");
            results = results.Replace("\\", "_");
            results = results.Replace(":", ";");
            results = results.Replace("*", "#");
            results = results.Replace("?", "@");
            results = results.Replace("\"", "'");
            results = results.Replace("<", "[");
            results = results.Replace(">", "]");
            results = results.Replace("|", "!");
            if (results.Length > length)
            {
                results = results.Substring(0, length);
            }

            return results;
        }

        private HandleResult receiveEvent(StartSupportAsset curEvent)
        {
            return new SuccessHandleResult();
        }

        private void getInfoFromTagsOrDesc( string tags, string desc, ref int assetTaskCount, ref string projectId )
        {
            // parsing Sitecore35 style metadata
            if ( isSitecore35_CQ15_Encoding(desc) )
            {
                string[] descElems = desc.Split(new char[] { '|' });
                if ( descElems.Length >= 3 )
                {
                    try
                    {
                        assetTaskCount = int.Parse(descElems[2]);
                    }
                    catch ( Exception e )
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, "Cannot parse project asset task count from " +
                            desc + ":" + e.Message);
                        assetTaskCount = 1;
                    }
                }
                if ( descElems.Length >= 4 )
                {
                    projectId = descElems[3];
                }
            }
            // parsing CQ5-general style metadata
            if ( isCQ5General_Encoding(tags) )
            {
                string[] tagElems = tags.Split(new char[] { '|' });
                if ( tagElems.Length >= 2 )
                {
                    projectId = tagElems[1];
                }
                if ( tagElems.Length >= 3 )
                {
                    try
                    {
                        assetTaskCount = int.Parse(tagElems[2]);
                    }
                    catch ( Exception e )
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, "Cannot parse project asset task count from " +
                            tags + ":" + e.Message);
                        assetTaskCount = 1;
                    }
                }
            }
        }

        private bool isSitecore35_CQ15_Encoding(String tags)
        {
            foreach (string encodingName in CQ15_SC_35_ENCODINGS)
            {
                if (tags.StartsWith(encodingName))
                {
                    return true;
                }
            }
            return false;
        }

        private bool isCQ5General_Encoding(String tags)
        {
            foreach (string encodingName in CQ5GENERAL_ENCODINGS)
            {
                if (tags.StartsWith(encodingName))
                {
                    return true;
                }
            }
            return false;
        }

        public static String truncateToByteLength(String input, int maxLength)
        {
            for (int i = input.Length - 1; i >= 0; i--)
            {
                if (Encoding.UTF8.GetByteCount(input.Substring(0, i + 1)) <= maxLength)
                {
                    return input.Substring(0, i + 1);
                }
            }

            return String.Empty;
        }


    }
}
