using System;
using System.Collections.Generic;
using System.Text; 
using Xstream.Core;

namespace ClayTablet.MemoQ
{
    public class ProviderJobMapping
    {

        //This is a sample mapping file, you can modify to provide your own mapping (depending on the TMS)
        private String localFileGUID;
	    private String tmsProjectGUID; 
	    private String tmsDocumentGUID; 
	    private String tmsTranslationStatus;
    	
	    private String projectId;
	    private String assetId; 
	    private String assetTaskId; 
	    private String fileExt;
    	 
	    private String filename = null;   
	    private String sourceTmsLanguageCode = null;
	    private String sourceCttLanguageCode = null;	
	    private String targetTmsLanguageCode = null;
	    private String targetCttLanguageCode = null; 
	    private String customReference;	
	    private String serverUrl = null; 
	    private String loginName = null;
	    private String loginPassword = null;
        private String dropfolder = null;
        private String checkfolder = null;

        private EventType receivedEventType;

	    /**
	     * Empty constructor.
	     * 
	     */

        public EventType ReceivedEventType
        {
            get { return this.receivedEventType; }
            set { this.receivedEventType = value; }
        }

	    public ProviderJobMapping() {
	    }
    	 	
	    public String getTmsTranslationStatus()
	    {		
		    return tmsTranslationStatus;
	    }
    	
	    public void setTmsTranslationStatus(String tmsTranslationStatus)
	    {		
		    this.tmsTranslationStatus = tmsTranslationStatus;
	    }
    	
    	
	    public String getTmsProjectGUID()
	    {		
		    return tmsProjectGUID;
	    }
    	
	    public void setTmsProjectGUID(String tmsProjectGUID)
	    {		
		    this.tmsProjectGUID = tmsProjectGUID;
	    }
    	
	    public String getLocalFileGUID()
	    {		
		    return localFileGUID;
	    }
    	
	    public void setLocalFileGUID(String localFileGUID)
	    {		
		    this.localFileGUID = localFileGUID;
	    }
    	
	    public String getTmsDocumentGUID()
	    {		
		    return tmsDocumentGUID;
	    }
    	
	    public void setTmsDocumentGUID(String tmsDocumentGUID)
	    {		
		    this.tmsDocumentGUID = tmsDocumentGUID;
	    }
    	 
     
	    /**
	     * @return the assetTaskId
	     */
	    public String getAssetTaskId() {
		    return assetTaskId;
	    }
    	
	    public String getAssetId() {
		    return assetId;
	    }
    	
	    public String getProjectId() {
		    return projectId;
	    }
     
	    /**
	     * @return the fileExt
	     */
	    public String getFileExt() {
		    return fileExt;
	    }

    	 
	    /**
	     * @param assetTaskId
	     *            the assetTaskId to set
	     */
	    public void setAssetTaskId(String assetTaskId) {
		    this.assetTaskId = assetTaskId;
	    }
    	
	    public void setAssetId(String assetId) {
		    this.assetId = assetId;
	    }
    	
	    public void setProjectId(String projectId) {
		    this.projectId = projectId;
	    }

	    /**
	     * @param fileExt
	     *            the fileExt to set
	     */
	    public void setFileExt(String fileExt) {
		    this.fileExt = fileExt;
	    }

    	 
    	
	    public String getFileName()
	    {		
		    return filename;
	    }
    	
	    public void setFileName(String filename)
	    {		
		    this.filename = filename;
	    }
 

        public String getDropfolder()
        {
            return dropfolder;
        }

        public void setDropfolder(String dropfolder)
        {
            this.dropfolder = dropfolder;
        }

        public String getCheckfolder()
        {
            return checkfolder;
        }

        public void setCheckfolder(String checkfolder)
        {
            this.checkfolder = checkfolder;
        }
 
    	
	    public String getSourceCttLanguageCode()
	    {		
		    return sourceCttLanguageCode;
	    }
    	
	    public void setSourceCttLanguageCode(String sourceCttLanguageCode)
	    {		
		    this.sourceCttLanguageCode = sourceCttLanguageCode;
	    }
    	
	    public String getTargetCttLanguageCode()
	    {		
		    return targetCttLanguageCode;
	    }
    	
	    public void setTargetCttLanguageCode(String targetCttLanguageCode)
	    {		
		    this.targetCttLanguageCode = targetCttLanguageCode;
	    }
    		
	    public String getSourceTmsLanguageCode()
	    {		
		    return sourceTmsLanguageCode;
	    }
    	
	    public void setSourceTmsLanguageCode(String sourceTmsLanguageCode)
	    {		
		    this.sourceTmsLanguageCode = sourceTmsLanguageCode;
	    }
    	
	    public String getTargetTmsLanguageCode()
	    {		
		    return targetTmsLanguageCode;
	    }
    	
	    public void setTargetTmsLanguageCode(String targetTmsLanguageCode)
	    {		
		    this.targetTmsLanguageCode = targetTmsLanguageCode;
	    }
    	
	    public String getCustomReference()
	    {		
		    return customReference;
	    }
    	
	    public void setCustomReference(String customReference)
	    {		
		    this.customReference = customReference;
	    }
    	  
	    public void setServerUrl(String serverUrl)
	    {		
		    this.serverUrl = serverUrl;
	    }
    	
	    public String getServerUrl()
	    {		
		    return this.serverUrl;
	    }
    	 
	    public void setLoginName(String loginName)
	    {		
		    this.loginName = loginName;
	    }
    	
	    public String getLoginName()
	    {		
		    return this.loginName;
	    }
    	
	    public void setLoginPassword(String loginPassword)
	    {		
		    this.loginPassword = loginPassword;
	    }
    	
	    public String getLoginPassword()
	    {		
		    return this.loginPassword;
	    }
    	
	    public static ProviderJobMapping fromXml(String xml)  {

		    // deserialize the account
		    return (ProviderJobMapping) getXStream().FromXml(xml);
	    }
    	
	    public static String toXml(ProviderJobMapping jobMapping)  {
     
		    // serilize the object to xml and return it
		    return getXStream().ToXml(jobMapping);
	    }
    	
	    private static XStream getXStream() {

		    XStream xstream = new XStream(); 
		    xstream.Alias("mapping", typeof(ProviderJobMapping) );

		    return xstream;
	    }

        public bool getisTranslated()
        {
            if (getTmsTranslationStatus() == "1") {
                return true;
            }
            return false;
        }

        public void setisTranslated(bool v)
        {
            if (v == true)
            {
                setTmsTranslationStatus("1");
            }
            else
            {
                setTmsTranslationStatus("0");
            }
        }
    }

    public enum EventType
    {
        NewTranslation = 0x01,
        UpdateTM = 0x02,
        TranslationUpdate = 0x04,
        TranslationFix = 0x08,
    }
}
