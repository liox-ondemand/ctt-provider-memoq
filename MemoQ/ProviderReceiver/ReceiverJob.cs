using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using com.claytablet.model;
using com.claytablet.provider;
using com.claytablet.model.enm;
using com.claytablet.model.Event;
using com.claytablet.model.Event.platform;
using com.claytablet.model.Event.provider;
using com.claytablet.service.Event;
using com.claytablet.util;
using com.claytablet.queue;
using com.claytablet.queue.service;
using com.claytablet.queue.service.sqs;
using com.claytablet.storage;
using com.claytablet.storage.service;
using com.claytablet.storage.service.s3;
using ClayTablet.CT.Utility;

namespace ClayTablet.MemoQ
{
    public class ReceiverJob
    {
        private QueueSubscriberService queueSubscriberService;
        private QueuePublisherService queuePublisherService;
        private StorageClientService storageClientService;
        private MyProviderReceiver receiver;
        private ProviderSender sender;

        bool needEmailNotification = false;
        String fromEmail = null;
        String smtpServer = null;
        int smtpPort = 25;
        bool smtpEnableSSL = false;
        String smtpUser = null;
        String smtpPassword = null;
        List<string> toEmails = new List<string>();

        public ReceiverJob()
        {

            SourceAccountProvider sap;
            TargetAccountProvider tap;

            StorageDirectoryProvider context;

            /*
            * sample config setting
            * 
            * Check the app.config in the sample
            * for web applications,  config goes web.config file.
            * 
            * when config the three keys below, please use absolute path
            * Since CT 2.0 may be used in web applecations, absolute path (can be figured outside of web folder)
            * to keep account info and connecitonContent info more secure.
            * 
                <appSettings>
		            <add key="ClayTablet.Provider.SourceAccount" value="E:\ctt_Core2.0_NET\CT2.0_Samples\ProviderSample\accounts\source.xml" />
		            <add key="ClayTablet.Provider.TargetAccount" value="E:\ctt_Core2.0_NET\CT2.0_Samples\ProviderSample\accounts\target.xml" />
		            <add key="ClayTablet.Provider.ContextFolder" value="E:\ctt_Core2.0_NET\CT2.0_Samples\ProviderSample\data\" />
	            </appSettings>
            * 
            *  
            */
            CLogger.WriteLog(ELogLevel.DEBUG, "Receiver processing messages");

            if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.Required"] == null)
            {
                needEmailNotification = false;
            }
            else
            {
                string needEmailNotification_setting = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.Required"].ToString().ToLower();

                if (needEmailNotification_setting.Equals("true"))
                    needEmailNotification = true;
                else
                    needEmailNotification = false;
            }

            if (needEmailNotification)
            {

                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.From.EmailAddress"] == null)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Can't find Form Email config [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Notification.From.EmailAddress] in AppSetting .");
                    return;
                }
                else
                {
                    fromEmail = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.From.EmailAddress"].ToString();
                }

                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Server"] == null)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Can't find SMTP server config [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.SMTP.Server] in AppSetting .");
                    return;
                }
                else
                {
                    smtpServer = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Server"].ToString();
                }


                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Port"] != null)
                {

                    try
                    {
                        smtpPort = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Port"].ToString());
                    }
                    catch (Exception)
                    {
                        smtpPort = 25;
                    }

                }

                smtpEnableSSL = false;
                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.EnableSSL"] != null)
                {
                    string EnableSSL_setting = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.EnableSSL"].ToString().ToLower();

                    if (EnableSSL_setting.Equals("true"))
                        smtpEnableSSL = true;
                    else
                        smtpEnableSSL = false;
                }

                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.UserName"] != null)
                {
                    smtpUser = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.UserName"].ToString();
                }

                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Password"] != null)
                {
                    smtpPassword = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Password"].ToString();
                }
            }

            var sourceAccountConfig = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SourceAccount"];
            String sourceAccountFile = null;
            if (sourceAccountConfig == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Source Account file [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Provider.SourceAccount] in AppSetting to point a source Account file and make sure system have read permission with the Account file.");
                return;
            }
            else
            {
                sourceAccountFile = PathUtil.getFullPath4RelatedPath(sourceAccountConfig.ToString());
            }

            String targetAccountFile = null;
            var targetAccountConfig = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.TargetAccount"];
            if (targetAccountConfig == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Target Account file [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Provider.TargetAccount] in AppSetting to point a target Account file and make sure system have read permission with the Account file.");
                return;
            }
            else
            {
                targetAccountFile = PathUtil.getFullPath4RelatedPath(targetAccountConfig.ToString());
            }

            if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ContextFolder"]))
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Can't location the ConnectionContext Folder.\n\nPlease configure [ClayTablet.Provider.ContextFolder] in AppSetting to point a folder and make sure system have Full permission with the folder.");
                return;
            }
            String clientId = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ClientId"];

            context = ProviderAssetTaskTranslationMap.Instance;

            String sourceAccountKey = FileUtil.ReadStringFromFile(sourceAccountFile);

            //Initial a source Account,  SDK will look for the source account file which appsetting ("ClayTablet.Provider.SourceAccount") pointed.
            sap = new SourceAccountProvider(clientId, sourceAccountKey);

            String targetAccountKey = FileUtil.ReadStringFromFile(targetAccountFile);

            //Initial a target Account,  SDK will look for the source account file which appsetting ("ClayTablet.Provider.TargetAccount") pointed.         
            tap = new TargetAccountProvider(clientId, targetAccountKey);

            Account sourceAccount = sap.get();
            if (sourceAccount == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot initialize source account from " + sourceAccountFile);
                return;
            } else
            {
                CLogger.WriteLog(ELogLevel.INFO, "Initialized source account: " + sourceAccount.getId());
            }

            Account targetAccount = tap.get();

            if (targetAccount == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot initialize target account from " + targetAccountFile);
                return;
            }
            else
            {
                CLogger.WriteLog(ELogLevel.INFO, "Initialized target account: " + targetAccount.getId());
            }

            storageClientService = new StorageClientServiceS3();
            storageClientService.setPublicKey(sourceAccount.getPublicKey());
            storageClientService.setPrivateKey(sourceAccount.getPrivateKey());
            storageClientService.setStorageBucket(sourceAccount.getStorageBucket());

            queuePublisherService = new QueuePublisherServiceSQS();
            queuePublisherService.setPublicKey(sourceAccount.getPublicKey());
            queuePublisherService.setPrivateKey(sourceAccount.getPrivateKey());
            queuePublisherService.setEndpoint(sourceAccount.getQueueEndpoint());

            queueSubscriberService = new QueueSubscriberServiceSQS();
            queueSubscriberService.setPublicKey(sourceAccount.getPublicKey());
            queueSubscriberService.setPrivateKey(sourceAccount.getPrivateKey());
            queueSubscriberService.setEndpoint(sourceAccount.getQueueEndpoint());


            //Initial a ProviderSender, receiver may need to send Event back to response.
            sender = new ProviderSender(context, sap, tap, queuePublisherService, storageClientService);
            receiver = new MyProviderReceiver(context, storageClientService, sender);
        }

        public void Process()
        {
            CLogger.WriteLog(ELogLevel.INFO, string.Format("Check SQS message for Item need translation. Start at {0}\t", System.DateTime.Now.ToString()));

            //Retrieve all avaiable messages
            int maxMessages = 9;
            int roundCount = 0;
            Boolean noMoreMsg = false;

            while ( !noMoreMsg && roundCount < 100 )
            {

                com.claytablet.queue.model.Message[] messages = queueSubscriberService.receiveMessages(maxMessages);
                CLogger.WriteLog(ELogLevel.INFO, "Found " + messages.Length + " messages.");

                if (messages.Length > 0)
                {
                    noMoreMsg = false;
                    roundCount++;
                    int msg_Count = 0;
                    foreach (com.claytablet.queue.model.Message message in messages)
                    {
                        msg_Count++;
                        CLogger.WriteLog(ELogLevel.INFO, msg_Count.ToString() + ")Message Body:\n" + message.getBody());
 
                        try
                        {
                            
                                //deserializing, from xml to AbsEvent,  
                                IEvent curEvent = AbsEvent.fromXml(message.getBody());

                                //call your ProducerReceiver to handle the event
                                HandleResult curHandleResult = receiver.ReceiveEvent(curEvent);

                                if (curHandleResult.CanDeleteMessage)
                                {
                                    //Event handled, delete from Queue
                                    queueSubscriberService.deleteMessage(message);
                                    //Console.WriteLine("Message handled, so it can be deleted from queue.");
                                }

                                if (!curHandleResult.Success)
                                {
                                    CLogger.WriteLog(ELogLevel.INFO, "Event handling error.\nError Message:" + curHandleResult.ErrorMessage + "\n" + curHandleResult.Message);
                                }
                           

                        }
                        catch (Exception e)
                        {
                            CLogger.WriteLog(ELogLevel.INFO, e.Message);
                        }


                    }


                }
                else
                {
                    noMoreMsg = true;
                    roundCount = 0; 
                }
            }
            CLogger.WriteLog(ELogLevel.DEBUG, "Receiver finished processing messages");
        }

        private void SendNotificationEmail(String fromEmail, List<string> toEmails, string subject, string body, bool isHtmlFormat,
            String smtpServer, int smtpPort, bool smtpEnableSSL, String smtpUser, String smtpPassword)
        {
            try
            {

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.From = new System.Net.Mail.MailAddress(fromEmail);

                //char[] splitchar = { ',' };
                //String[] notifyEmailList = toEmails.Split(splitchar);
                //if (notifyEmailList != null && notifyEmailList.Length > 0)
                if (toEmails != null && toEmails.Count > 0)
                {
                    foreach (String emailAddress in toEmails)
                    {
                        try
                        {
                            mail.To.Add(new System.Net.Mail.MailAddress(emailAddress));
                        }
                        catch (Exception e)
                        {
                            CLogger.WriteLog(ELogLevel.ERROR, "Cannot add recipient: " + emailAddress + " error: " + e.Message);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                        }
                    }

                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = isHtmlFormat;
                    System.Net.Mail.SmtpClient smtpClient;

                    if (!string.IsNullOrEmpty(smtpServer))
                    {
                        smtpClient = new System.Net.Mail.SmtpClient(
                                smtpServer, smtpPort);

                        smtpClient.EnableSsl = smtpEnableSSL;

                        if (String.IsNullOrEmpty(smtpUser))
                        {
                            smtpClient.UseDefaultCredentials = true;
                        }
                        else
                        {
                            smtpClient.UseDefaultCredentials = false;
                            smtpClient.Credentials = new System.Net.NetworkCredential(
                                    smtpUser, smtpPassword);
                        }
                    }
                    else
                    {
                        smtpClient = new System.Net.Mail.SmtpClient();
                    }

                    smtpClient.Send(mail);
                    CLogger.WriteLog(ELogLevel.INFO, "Notification email sent successfully with smtp.");
                }
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Failed to send notification email with smtp.\nError:" + e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
            }
        }

        public void SendNotifications()
        {
            CLogger.WriteLog(ELogLevel.INFO, "[SendNotifications]  start ....");
            DateTime lastSent = LastEmailSending.GetLastSentTime();
            CLogger.WriteLog(ELogLevel.INFO, "Last time of sending : " + lastSent.ToString());
            string connectorServiceName = System.Configuration.ConfigurationManager.AppSettings["Connector_Service_Name"];

            try
            {
                List<NotifyEmail> priorityEmailList = new List<NotifyEmail>();

                List<NotifyEmail> notifyEmailList = NotifyEmail.ListAllNotifyEmails(priorityEmailList);

                if (priorityEmailList.Count > 0)
                {
                    CLogger.WriteLog(ELogLevel.INFO, "Get " + priorityEmailList.Count + " priority emails need to send out.");
                    sendEmails(priorityEmailList, "[Lionbridge CT3] Attention: Errors from " + connectorServiceName);
                }

                if (lastSent.CompareTo(DateTime.Now.AddMinutes(-10)) < 0)
                {
                    CLogger.WriteLog(ELogLevel.DEBUG, "Time passed,  need to pick all saved emails and send out");
                    if (notifyEmailList.Count == 0)
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "No email to send out");
                    }
                    else
                    {
                        sendEmails(notifyEmailList, "[Lionbridge CT3] New File(s) for Translation by " + connectorServiceName + ".]");
                    }

                    LastEmailSending.SaveLastSentTime(DateTime.Now);
                }
                else
                    CLogger.WriteLog(ELogLevel.DEBUG, "less then 10 Minutes. wait longer.");
            }
            catch (Exception e)
            {

                CLogger.WriteLog(ELogLevel.ERROR, "Error: " + e.Message);
                CLogger.WriteLog(ELogLevel.ERROR, "State Trace: " + e.StackTrace);

            }
            CLogger.WriteLog(ELogLevel.DEBUG, "[SendNotifications]  completed ....");
        }

        private void sendEmails(List<NotifyEmail> emailList, string subject)
        {
            if (emailList.Count > 0)
            {
                CLogger.WriteLog(ELogLevel.INFO, "Get " + emailList.Count + " saved emails need to send out.");

                String emailBody;
                if (!emailList[0].IsPriority)
                {
                    emailBody = "Hello, \n Lionbridge MemoQ connector has uploaded " + emailList.Count + " file(s) for translation.\n\n";
                }
                else
                {
                    emailBody = "Hello, \n Lionbridge MemoQ connector has encountered error when uploading " + emailList.Count + " file(s) for translation.\n\n";
                }
                int i = 0;

                string tns_NotifyEmail = null;
                List<NotifyEmail> deleteEmailList = new List<NotifyEmail>();
                foreach (NotifyEmail notifyEmail in emailList)
                {
                    if (tns_NotifyEmail == null)
                        tns_NotifyEmail = notifyEmail.EmailAddress;

                    if (String.Compare(tns_NotifyEmail, notifyEmail.EmailAddress, false) == 0)
                    {
                        emailBody = emailBody + (i + 1) + ".\n" + notifyEmail.EmailBody + "\n";
                        deleteEmailList.Add(notifyEmail);
                    }
                }

                emailBody += "\nThis is an automatic notification email.\n";
                emailBody += "If you feel you are receiving this email in error, please contact the system administrator at: admin@clay-tablet.com \n\n\n";
                emailBody += "==========================\n";
                emailBody += "Generated by:\n";
                emailBody += "ClayTable CT3.0\n\n";

                emailBody += "Clay Tablet Technologies\n";
                emailBody += "www.Clay-Tablet.com\n";
                emailBody += "Simply Global.\n";
                emailBody += "==========================\n\n";

                try
                {
                    char[] splitchar = { ',' };
                    List<string> toEmails = new List<string>(tns_NotifyEmail.Split(splitchar));

                    this.SendNotificationEmail(fromEmail, toEmails, subject, emailBody, false,
                        smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                    CLogger.WriteLog(ELogLevel.INFO, "Send to: " + tns_NotifyEmail + " via " + smtpServer + " ... success!");

                    foreach (NotifyEmail notifyEmail in deleteEmailList)
                    {
                        notifyEmail.Delete();
                    }
                }
                catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Send to: " + tns_NotifyEmail + " ... Failed!");
                    CLogger.WriteLog(ELogLevel.INFO, "-----------------Error Message--------------");
                    CLogger.WriteLog(ELogLevel.INFO, e.Message);
                    CLogger.WriteLog(ELogLevel.INFO, "-----------------Stack Trace----------------");
                    CLogger.WriteLog(ELogLevel.INFO, e.StackTrace);
                    CLogger.WriteLog(ELogLevel.INFO, "--------------------------------------------");

                    if (sender != null)
                    {
                        foreach (NotifyEmail email in deleteEmailList)
                        {
                            try
                            {
                                AssetTaskProviderError ev = email.EventToSendWhenFail;
                                if (ev != null)
                                {
                                    sender.sendEvent(ev);
                                    email.EventToSendWhenFail = null;
                                    email.Save();
                                }
                            }
                            catch (Exception exp)
                            {
                                CLogger.WriteLog(ELogLevel.ERROR, 
                                    "Cannot send event attached to notification " + email.Guid, exp);
                            }
                        }
                    }
                }
            }
        }

    }
    } 
