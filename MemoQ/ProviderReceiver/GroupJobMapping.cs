using System;
using System.Collections.Generic;
using System.Text; 
using Xstream.Core;

namespace ClayTablet.MemoQ
{
    public class GroupJobMapping
    {

        //This is a sample mapping file, you can modify to provide your own mapping (depending on the TMS)
        
	    private String sourceTmsLanguageCode = null; 
	    private String targetTmsLanguageCode = null;  
	    private String serverUrl = null; 
	    private String loginName = null;
	    private String loginPassword = null; 
        private String checkfolder = null;
     

	    /**
	     * Empty constructor.
	     * 
	     */
        public GroupJobMapping()
        {
	    }
    	  
 
        public String getCheckfolder()
        {
            return checkfolder;
        }

        public void setCheckfolder(String checkfolder)
        {
            this.checkfolder = checkfolder;
        }
 
    	 
    		
	    public String getSourceTmsLanguageCode()
	    {		
		    return sourceTmsLanguageCode;
	    }
    	
	    public void setSourceTmsLanguageCode(String sourceTmsLanguageCode)
	    {		
		    this.sourceTmsLanguageCode = sourceTmsLanguageCode;
	    }
    	
	    public String getTargetTmsLanguageCode()
	    {		
		    return targetTmsLanguageCode;
	    }
    	
	    public void setTargetTmsLanguageCode(String targetTmsLanguageCode)
	    {		
		    this.targetTmsLanguageCode = targetTmsLanguageCode;
	    }
    	
	     
    	  
	    public void setServerUrl(String serverUrl)
	    {		
		    this.serverUrl = serverUrl;
	    }
    	
	    public String getServerUrl()
	    {		
		    return this.serverUrl;
	    }
    	 
	    public void setLoginName(String loginName)
	    {		
		    this.loginName = loginName;
	    }
    	
	    public String getLoginName()
	    {		
		    return this.loginName;
	    }
    	
	    public void setLoginPassword(String loginPassword)
	    {		
		    this.loginPassword = loginPassword;
	    }
    	
	    public String getLoginPassword()
	    {		
		    return this.loginPassword;
	    }
    	
	    
    }
}
