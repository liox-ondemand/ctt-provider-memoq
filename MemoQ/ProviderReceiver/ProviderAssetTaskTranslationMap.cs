using System;
using System.IO;
using System.Collections.Generic; 
using System.Text;
using com.claytablet.model;
using com.claytablet.provider;
using com.claytablet.model.enm;
using com.claytablet.model.Event;
using com.claytablet.model.Event.platform;
using com.claytablet.model.Event.provider;
using com.claytablet.service.Event;
using com.claytablet.util;
using com.claytablet.queue;
using com.claytablet.queue.model;
using com.claytablet.queue.service;
using com.claytablet.queue.service.sqs;
using com.claytablet.storage;
using com.claytablet.storage.service;
using com.claytablet.storage.service.s3;
using ClayTablet.CT.Utility;

namespace ClayTablet.MemoQ
{
    public class ProviderAssetTaskTranslationMap : StorageDirectoryProvider
    {
        // sample Mapping handling,  used local file to save info
        private String dataDir = null;
        private String jobDataDir = null;
        private String jobArchiveDir = null;
        private String jobDumpsterDir = null;
        private string sourceDirectory = null;
        private string targetDirectory = null;

        public ProviderAssetTaskTranslationMap()
        {
            dataDir = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ContextFolder"];
            if (dataDir.EndsWith("\\"))
            {
                jobDataDir = dataDir + "jobs\\";
                sourceDirectory = dataDir + "files\\source\\";
                targetDirectory = dataDir + "files\\target\\";
            }
            else
            {
                jobDataDir = dataDir + "\\jobs\\";
                sourceDirectory = dataDir + "\\files\\source\\";
                targetDirectory = dataDir + "\\files\\target\\";
            }

            jobArchiveDir = jobDataDir + "archive\\";
            jobDumpsterDir = jobDataDir + "..\\dumpster\\";


            if (!System.IO.Directory.Exists(jobDataDir))
                System.IO.Directory.CreateDirectory(jobDataDir);
            if (!System.IO.Directory.Exists(jobArchiveDir))
                System.IO.Directory.CreateDirectory(jobArchiveDir);
            if (!System.IO.Directory.Exists(jobDumpsterDir))
                System.IO.Directory.CreateDirectory(jobDumpsterDir);
            if (!System.IO.Directory.Exists(sourceDirectory))
                System.IO.Directory.CreateDirectory(sourceDirectory);
            if (!System.IO.Directory.Exists(targetDirectory))
                System.IO.Directory.CreateDirectory(targetDirectory);
        }

        public static ProviderAssetTaskTranslationMap Instance
        {
            get
            {
                return Nested.Instance;
            }
        }

        private class Nested
        {
            private static ProviderAssetTaskTranslationMap instance;

            static Nested()
            {
                instance = new ProviderAssetTaskTranslationMap();
            }

            public static ProviderAssetTaskTranslationMap Instance
            {
                get
                {
                    return instance;
                }
            }

        }

        public string getSourceDirectory()
        {
            return sourceDirectory;
        }

        public string getTargetDirectory()
        {
            return targetDirectory;
        }

        public List<ProviderJobMapping> ListJobs() 
        {

		        // Retrieve the xml data directory where the mappings are stored  
                List<ProviderJobMapping> jobs  = new List<ProviderJobMapping>();	 

                String[] exts = { "xml" };
		        //Console.WriteLine("Load and deserialize all of the JobMapping from " + DEFAULT_DATA_DIR_JOB);
		        List<String> jobFiles = FileUtil.ListFiles(jobDataDir, exts);
                foreach (String file in jobFiles)
                {
			        ProviderJobMapping job = ProviderJobMapping.fromXml(FileUtil.ReadStringFromFile(file));
			        jobs.Add(job);
		        }
    		 
		    return jobs;
    		 
	    }

        public ProviderJobMapping SearchByGroupMapAndFileName(GroupJobMapping groupJobMapping, String fileName)
        {
            List<ProviderJobMapping> jobs = ListJobs();

            foreach (ProviderJobMapping job in jobs)
            {
                if (   String.Compare(job.getFileName(), fileName,true)==0
                    && String.Compare(job.getCheckfolder(), groupJobMapping.getCheckfolder(), true) == 0
                    && String.Compare(job.getServerUrl(), groupJobMapping.getServerUrl(), true) == 0
                    && String.Compare(job.getLoginName(), groupJobMapping.getLoginName(), true) == 0
                    && String.Compare(job.getLoginPassword(), groupJobMapping.getLoginPassword(), true) == 0
                    && String.Compare(job.getSourceTmsLanguageCode(), groupJobMapping.getSourceTmsLanguageCode(), true) == 0
                    && String.Compare(job.getTargetTmsLanguageCode(), groupJobMapping.getTargetTmsLanguageCode(), true) == 0
                     
                    )
                {
                    return job; 
                }
            }

            return null;
        }
    	 
	    public String SearchSameTmsProjectGUID(String cttProjectId, string cttTargetLanguageCode) 
            //Search if an CTT project had been handled already 
        {
            
		    // Retrieve the xml data directory where the mappings are stored 
            String returnTmsProjectGUID = null;
		    List<ProviderJobMapping> jobs  = ListJobs();
    		  
		    foreach (ProviderJobMapping job in jobs) {
                if ( cttProjectId.Equals(job.getProjectId()) && cttTargetLanguageCode.Equals(job.getTargetCttLanguageCode()) )
                {
				    returnTmsProjectGUID =  job.getTmsProjectGUID();
                    break;
                }
		    }
    		 
		    return returnTmsProjectGUID;
    		 
	    }

        public List<ProviderJobMapping> SearchTasksByTmsProjectGUID( String tmsProjectGUID )
        //Search all handled tasks in one tms project
        {
            // Retrieve the xml data directory where the mappings are stored 
            List<ProviderJobMapping> jobs = ListJobs();
            List<ProviderJobMapping> mappings = new List<ProviderJobMapping>();

            foreach ( ProviderJobMapping job in jobs )
            {
                if ( tmsProjectGUID.Equals(job.getTmsProjectGUID()) )
                {
                    mappings.Add(job);
                }
            }

            return mappings;
        }

        public List<ProviderJobMapping> SearchTasksByTmsProjectGUID( String tmsProjectGUID, List<ProviderJobMapping> cttProjectIdJobMappings )
        //Search all handled tasks in one tms project
        {
            List<ProviderJobMapping> mappings = new List<ProviderJobMapping>();

            foreach ( ProviderJobMapping job in cttProjectIdJobMappings )
            {
                if ( tmsProjectGUID.Equals(job.getTmsProjectGUID()) )
                {
                    mappings.Add(job);
                }
            }

            return mappings;
        }

        public List<ProviderJobMapping> SearchTasksByCttProjectId( String cttProjectId )
        {
            // Retrieve the xml data directory where the mappings are stored 
            List<ProviderJobMapping> jobs = ListJobs();
            List<ProviderJobMapping> mappings = new List<ProviderJobMapping>();

            foreach ( ProviderJobMapping job in jobs )
            {
                if ( cttProjectId.Equals(job.getProjectId()) )
                {
                    mappings.Add(job);
                }
            }

            return mappings;
        }

        public List<string> SearchTmsProjectGUIDListByCttProjectId( String cttProjectId )
        {
            // Retrieve the xml data directory where the mappings are stored 
            List<ProviderJobMapping> jobs = ListJobs();
            List<string> tmsProjectGUIDList = new List<string>();
            foreach ( ProviderJobMapping job in jobs )
            {
                if ( cttProjectId.Equals(job.getProjectId()) )
                {
                    string tmsProjectGUID = job.getTmsProjectGUID();
                    if ( !tmsProjectGUIDList.Contains(tmsProjectGUID) )
                    {
                        tmsProjectGUIDList.Add(tmsProjectGUID);
                    }
                }
            }
            return tmsProjectGUIDList;
        }

        public List<string> SearchTmsProjectGUIDListByCttProjectId( String cttProjectId, List<ProviderJobMapping> cttProjectIdJobMappings )
        {
            List<string> tmsProjectGUIDList = new List<string>();
            foreach ( ProviderJobMapping job in cttProjectIdJobMappings )
            {
                if ( cttProjectId.Equals(job.getProjectId()) )
                {
                    string tmsProjectGUID = job.getTmsProjectGUID();
                    if ( !tmsProjectGUIDList.Contains(tmsProjectGUID) )
                    {
                        tmsProjectGUIDList.Add(tmsProjectGUID);
                    }
                }
            }
            return tmsProjectGUIDList;
        }

        public ProviderJobMapping findJobByAssetTaskId(String assetTaskId) {
            String jobFile = jobDataDir + assetTaskId + ".job.xml";

            if (File.Exists(jobFile))
            {
                return ProviderJobMapping.fromXml(FileUtil.ReadStringFromFile(jobFile));
            }

            foreach (ProviderJobMapping mapping in ListJobs())
            {
				if (mapping.getAssetTaskId().Equals(assetTaskId)) 
				{
					return mapping;   					 
				}
			}
            return null;
        }
    	 
	    public void Add(ProviderJobMapping job)  
        {
		    // save to the xml data directory where the JobMapping should stored
            String guid = IdGenerator.createId();
            job.setLocalFileGUID(guid);

            String jobFile = jobDataDir + job.getAssetTaskId() + ".job.xml";

            FileUtil.WriteStringToFile(jobFile , ProviderJobMapping.toXml(job)); 
	    }

    	
	    public void Remove(ProviderJobMapping job)  
        {
		    // remove a job mapping file	    	     
            String jobFile = jobDataDir + job.getAssetTaskId() + ".job.xml";
            String jobRemvoedFile = jobDumpsterDir + job.getAssetTaskId() + ".job.xml";
            if (File.Exists(jobFile))
                File.Move(jobFile, jobRemvoedFile);
            String guid = job.getLocalFileGUID();
            String jobOldFile = jobDataDir + guid + ".xml";
            if (File.Exists( jobOldFile )  )
                File.Move(jobOldFile, jobRemvoedFile);
            File.SetLastWriteTime(jobRemvoedFile, DateTime.Now);
            RemoveSourceAndTargetFiles(job);
        }


        public void Archive(ProviderJobMapping job)
        {
            String guid = job.getLocalFileGUID();
            String jobFile = jobDataDir + job.getAssetTaskId() + ".job.xml";
            String jobArchiveFile = jobArchiveDir + job.getAssetTaskId() + ".job.xml";
            if (File.Exists(jobFile))
            {
                File.Move(jobFile, jobArchiveFile);
            }
            jobFile = jobDataDir + guid + ".xml";
            if (File.Exists(jobFile))
            {
                File.Move(jobFile, jobArchiveFile);
            }
        }

        public void RemoveArchive(String assetTaskId)
        {
            String jobArchiveFile = jobArchiveDir + assetTaskId + ".job.xml";
            String jobRemvoedFile = jobDumpsterDir + assetTaskId + ".job.xml";
            if (File.Exists(jobArchiveFile))
            {
                ProviderJobMapping job = ProviderJobMapping.fromXml(FileUtil.ReadStringFromFile(jobArchiveFile));
                RemoveSourceAndTargetFiles(job);
                SafeMove(jobArchiveFile, jobRemvoedFile);
            }
        }

        private void RemoveSourceAndTargetFiles(ProviderJobMapping job)
        {
            string sourceFile = getSourceDirectory() + job.getFileName();
            string sourceDumpsterFile = jobDumpsterDir + "\\" + job.getFileName() + ".source";

            SafeMove(sourceFile, sourceDumpsterFile);

            string targetFile = getTargetDirectory() + "\\" + job.getFileName();
            string targetDumpsterFile = jobDumpsterDir + "\\" + job.getFileName() + ".target";

            SafeMove(targetFile, targetDumpsterFile);
        }

        private static void SafeMove(string sourceFile, string targetFile)
        {
            if (File.Exists(sourceFile))
            {
                string originalTargetFile = targetFile;
                while (File.Exists(targetFile))
                {
                    System.Threading.Thread.Sleep(1);
                    DateTime now = DateTime.Now;
                    targetFile = originalTargetFile + "." + now.Year + "_" + now.Month + "_" + now.Day +
                            "_" + now.Hour + "_" + now.Minute + "_" + now.Second + "_" + now.Millisecond;
                }
                try
                {
                    File.Move(sourceFile, targetFile);
                    File.SetLastWriteTime(targetFile, DateTime.Now);
                }
                catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Cannot move " + sourceFile + " to " + targetFile, e);
                }
            }
        }
    }
}
