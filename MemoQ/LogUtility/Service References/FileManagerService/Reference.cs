﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Utility.FileManagerService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="UnexpectedFault", Namespace="http://kilgray.com/memoqservices/2007")]
    [System.SerializableAttribute()]
    public partial class UnexpectedFault : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StackTraceField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StackTrace {
            get {
                return this.StackTraceField;
            }
            set {
                if ((object.ReferenceEquals(this.StackTraceField, value) != true)) {
                    this.StackTraceField = value;
                    this.RaisePropertyChanged("StackTrace");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="GenericFault", Namespace="http://kilgray.com/memoqservices/2007")]
    [System.SerializableAttribute()]
    public partial class GenericFault : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ErrorCodeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ErrorCode {
            get {
                return this.ErrorCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.ErrorCodeField, value) != true)) {
                    this.ErrorCodeField = value;
                    this.RaisePropertyChanged("ErrorCode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://kilgray.com/memoqservices/2007", ConfigurationName="FileManagerService.IFileManagerService")]
    public interface IFileManagerService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://kilgray.com/memoqservices/2007/IFileManagerService/BeginChunkedFileUpload", ReplyAction="http://kilgray.com/memoqservices/2007/IFileManagerService/BeginChunkedFileUploadR" +
            "esponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.UnexpectedFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/BeginChunkedFileUploadU" +
            "nexpectedFaultFault", Name="UnexpectedFault")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.GenericFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/BeginChunkedFileUploadG" +
            "enericFaultFault", Name="GenericFault")]
        System.Guid BeginChunkedFileUpload(string fileName, bool isZipped);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://kilgray.com/memoqservices/2007/IFileManagerService/AddNextFileChunk", ReplyAction="http://kilgray.com/memoqservices/2007/IFileManagerService/AddNextFileChunkRespons" +
            "e")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.UnexpectedFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/AddNextFileChunkUnexpec" +
            "tedFaultFault", Name="UnexpectedFault")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.GenericFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/AddNextFileChunkGeneric" +
            "FaultFault", Name="GenericFault")]
        void AddNextFileChunk(System.Guid fileIdAndSessionId, byte[] fileData);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://kilgray.com/memoqservices/2007/IFileManagerService/EndChunkedFileUpload", ReplyAction="http://kilgray.com/memoqservices/2007/IFileManagerService/EndChunkedFileUploadRes" +
            "ponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.UnexpectedFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/EndChunkedFileUploadUne" +
            "xpectedFaultFault", Name="UnexpectedFault")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.GenericFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/EndChunkedFileUploadGen" +
            "ericFaultFault", Name="GenericFault")]
        void EndChunkedFileUpload(System.Guid fileIdAndSessionId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://kilgray.com/memoqservices/2007/IFileManagerService/BeginChunkedFileDownloa" +
            "d", ReplyAction="http://kilgray.com/memoqservices/2007/IFileManagerService/BeginChunkedFileDownloa" +
            "dResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.UnexpectedFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/BeginChunkedFileDownloa" +
            "dUnexpectedFaultFault", Name="UnexpectedFault")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.GenericFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/BeginChunkedFileDownloa" +
            "dGenericFaultFault", Name="GenericFault")]
        System.Guid BeginChunkedFileDownload(out string fileName, out int fileSize, System.Guid fileGuid, bool zip);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://kilgray.com/memoqservices/2007/IFileManagerService/GetNextFileChunk", ReplyAction="http://kilgray.com/memoqservices/2007/IFileManagerService/GetNextFileChunkRespons" +
            "e")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.UnexpectedFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/GetNextFileChunkUnexpec" +
            "tedFaultFault", Name="UnexpectedFault")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.GenericFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/GetNextFileChunkGeneric" +
            "FaultFault", Name="GenericFault")]
        byte[] GetNextFileChunk(System.Guid sessionId, int byteCount);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://kilgray.com/memoqservices/2007/IFileManagerService/EndChunkedFileDownload", ReplyAction="http://kilgray.com/memoqservices/2007/IFileManagerService/EndChunkedFileDownloadR" +
            "esponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.UnexpectedFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/EndChunkedFileDownloadU" +
            "nexpectedFaultFault", Name="UnexpectedFault")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.GenericFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/EndChunkedFileDownloadG" +
            "enericFaultFault", Name="GenericFault")]
        void EndChunkedFileDownload(System.Guid sessionId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://kilgray.com/memoqservices/2007/IFileManagerService/DeleteFile", ReplyAction="http://kilgray.com/memoqservices/2007/IFileManagerService/DeleteFileResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.UnexpectedFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/DeleteFileUnexpectedFau" +
            "ltFault", Name="UnexpectedFault")]
        [System.ServiceModel.FaultContractAttribute(typeof(Utility.FileManagerService.GenericFault), Action="http://kilgray.com/memoqservices/2007/IFileManagerService/DeleteFileGenericFaultF" +
            "ault", Name="GenericFault")]
        void DeleteFile(System.Guid fileGuid);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IFileManagerServiceChannel : Utility.FileManagerService.IFileManagerService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class FileManagerServiceClient : System.ServiceModel.ClientBase<Utility.FileManagerService.IFileManagerService>, Utility.FileManagerService.IFileManagerService {
        
        public FileManagerServiceClient() {
        }
        
        public FileManagerServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public FileManagerServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FileManagerServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FileManagerServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Guid BeginChunkedFileUpload(string fileName, bool isZipped) {
            return base.Channel.BeginChunkedFileUpload(fileName, isZipped);
        }
        
        public void AddNextFileChunk(System.Guid fileIdAndSessionId, byte[] fileData) {
            base.Channel.AddNextFileChunk(fileIdAndSessionId, fileData);
        }
        
        public void EndChunkedFileUpload(System.Guid fileIdAndSessionId) {
            base.Channel.EndChunkedFileUpload(fileIdAndSessionId);
        }
        
        public System.Guid BeginChunkedFileDownload(out string fileName, out int fileSize, System.Guid fileGuid, bool zip) {
            return base.Channel.BeginChunkedFileDownload(out fileName, out fileSize, fileGuid, zip);
        }
        
        public byte[] GetNextFileChunk(System.Guid sessionId, int byteCount) {
            return base.Channel.GetNextFileChunk(sessionId, byteCount);
        }
        
        public void EndChunkedFileDownload(System.Guid sessionId) {
            base.Channel.EndChunkedFileDownload(sessionId);
        }
        
        public void DeleteFile(System.Guid fileGuid) {
            base.Channel.DeleteFile(fileGuid);
        }
    }
}
