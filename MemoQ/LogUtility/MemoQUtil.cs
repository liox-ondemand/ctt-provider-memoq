﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Xml;
using ClayTablet.CT.Utility;
using Utility.FileManagerService;
using Utility.ResourceService;
using Utility.SecurityService;
using Utility.ServerProjectService;
using Utility.TBService;
using Utility.TMService;
using System.Net;

namespace Utility
{
    public class MemoQUtil
    {
        private static string salt = "fgad s d f sgds g  sdg gfdg";

        static MemoQUtil()
        {

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

        }

        // key is CTTLanguageCode, value is MemoQLanguageCode
        private static Dictionary<string, string> GetLanguageCodeMapping()
        {
            Dictionary<string, string> dictLanguageCodeMapping = new Dictionary<string, string>();
            // Read XML
            string languageCodeMappingFilePath = AppDomain.CurrentDomain.BaseDirectory + @"Data\LanguageCodeMapping.xml";
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(languageCodeMappingFilePath);
                foreach (XmlNode xn in document["LanguageCodeMappings"].ChildNodes)
                {
                    string cttLanguageCode = string.Empty;
                    string memoQLanguageCode = string.Empty;
                    if (xn.NodeType != XmlNodeType.Comment)
                    {
                        if (xn["CTTLanguageCode"] != null)
                        {
                            cttLanguageCode = xn["CTTLanguageCode"].InnerText;
                        }
                        if (xn["MemoQLanguageCode"] != null)
                        {
                            memoQLanguageCode = xn["MemoQLanguageCode"].InnerText;
                        }
                        if (!dictLanguageCodeMapping.ContainsKey(cttLanguageCode))
                        {
                            dictLanguageCodeMapping.Add(cttLanguageCode, memoQLanguageCode);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, string.Format("Can't find language code mapping xml in {0} or xml is in invalid format", languageCodeMappingFilePath), e);
            }
            return dictLanguageCodeMapping;
        }

        /// <summary>
        /// Converts CT 2.0 language code to tms language code
        /// </summary>
        /// <param name="cttLanguageCode"></param>
        /// <returns></returns>
        public static string ConvertCTTLanguageCodeToMemoQLanguageCode(string cttLanguageCode)
        {
            string retStr = string.Empty;
            Dictionary<string, string> dictLanguageCodeMapping = GetLanguageCodeMapping();
            if (dictLanguageCodeMapping != null)
            {
                if (dictLanguageCodeMapping.ContainsKey(cttLanguageCode))
                {
                    retStr = dictLanguageCodeMapping[cttLanguageCode];
                }
            }
            return retStr;
        }

        public static string HashPassword(string password)
        {
            return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(password + salt, "sha1");
        }

        /// <summary>
        /// Gets users 
        /// </summary>
        /// <returns></returns>
        public static SecurityService.UserInfo[] ListUsers()
        {
            try
            {

                string password = string.Empty;
                string username = string.Empty;
                if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["MemoQ_LoginName"]))
                {

                    CLogger.WriteLog(ELogLevel.ERROR, "Can't location the memoQ login name [in Configuration.AppSettings].\n\nPlease configure [MemoQ_LoginName] in AppSetting.");
                    return null;
                }
                else
                {
                    username = System.Configuration.ConfigurationManager.AppSettings["MemoQ_LoginName"];
                }
                if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["MemoQ_LoginPassword"]))
                {

                    CLogger.WriteLog(ELogLevel.ERROR, "Can't location the memoQ login password [in Configuration.AppSettings].\n\nPlease configure [MemoQ_LoginPassword] in AppSetting.");
                    return null;
                }
                else
                {
                    password = System.Configuration.ConfigurationManager.AppSettings["MemoQ_LoginPassword"];
                }

                ISecurityService securityService = GetSecurityService();
                //securityService.Login(username, HashPassword(password));
                return securityService.ListUsers();
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot logon to security service", e);
                return null;
            }
        }

        #region Getting service objects

        /// <summary>
        /// Gets a ITMService service object. This is platform and technology
        /// specific.
        /// </summary>
        /// <returns></returns>
        public static ITMService GetTMService()
        {
            ChannelFactory<ITMService> channelFactory =
                new ChannelFactory<ITMService>("TMService");
            return channelFactory.CreateChannel();
        }

        /// <summary>
        /// Gets a ITBService service object. This is platform and technology
        /// specific.
        /// </summary>
        /// <returns></returns>
        public static ITBService GetTBService()
        {
            ChannelFactory<ITBService> channelFactory =
                new ChannelFactory<ITBService>("TBService");
            return channelFactory.CreateChannel();
        }

        /// <summary>
        /// Gets a ISecurityService service object. This is platform and technology
        /// specific.
        /// </summary>
        /// <returns></returns>
        public static ISecurityService GetSecurityService()
        {
            ChannelFactory<ISecurityService> channelFactory =
                new ChannelFactory<ISecurityService>("SecurityService");
            return channelFactory.CreateChannel();
        }

        /// <summary>
        /// Gets a IServerProjectService service object. This is platform and
        /// technology specific.
        /// </summary>
        /// <returns></returns>
        public static IServerProjectService GetServerProjectService()
        {
            ChannelFactory<IServerProjectService> channelFactory =
                new ChannelFactory<IServerProjectService>("ServerProjectService");
            return channelFactory.CreateChannel();
        }

        /// <summary>
        /// Gets an IResourceService service object. This is platform and
        /// technology specific.
        /// </summary>
        /// <returns></returns>
        public static IResourceService GetResourceService()
        {
            ChannelFactory<IResourceService> channelFactory =
                new ChannelFactory<IResourceService>("ResourceService");
            return channelFactory.CreateChannel();
        }

        /// <summary>
        /// Gets a IFileManagerService service object. This is platform and
        /// technology specific.
        /// </summary>
        /// <returns></returns>
        public static IFileManagerService GetFileManagerService()
        {
            ChannelFactory<IFileManagerService> channelFactory =
                new ChannelFactory<IFileManagerService>("FileManagerService");
            return channelFactory.CreateChannel();
        }

        #endregion

        #region file management
        /// <summary>
        /// Uploads the file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>The guid of the uploaded file.</returns>
        public static Guid UploadFile(string filePath)
        {
            FileStream fileStream = null;
            Guid fileGuid = Guid.Empty;
            int chunkSize = 1000000;
            byte[] chunkBytes = new byte[chunkSize];
            byte[] dataToUpload;
            int bytesRead;

            IFileManagerService fmService = GetFileManagerService();

            try
            {
                // Open souce file stream
                fileStream = File.OpenRead(filePath);

                // Begin chunked upload
                fileGuid = fmService.BeginChunkedFileUpload(filePath, false);

                // Upload chunks
                while ((bytesRead = fileStream.Read(chunkBytes, 0, chunkSize)) != 0)
                {
                    if (bytesRead == chunkSize)
                        dataToUpload = chunkBytes;
                    else
                    {
                        // If we are at the end, we want to upload only
                        // the bytes read from the stream.
                        dataToUpload = new byte[bytesRead];
                        Array.Copy(chunkBytes, dataToUpload, bytesRead);
                    }

                    fmService.AddNextFileChunk(fileGuid, dataToUpload);
                }

                return fileGuid;
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();

                // End chunked upload
                if (fileGuid != Guid.Empty)
                    fmService.EndChunkedFileUpload(fileGuid);
            }
        }

        /// <summary>
        /// Downloads the document.
        /// </summary>
        /// <param name="fileGuid">The file GUID.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="targetDir">The target directory.</param>
        public static void DownloadFile(Guid fileGuid, out string fileName, string targetDir)
        {
            FileStream fileStream = null;
            Guid sessionGuid = Guid.Empty;
            int chunkSize = 1000000;
            byte[] chunkBytes = new byte[chunkSize];
            int fileSize;
            int fileBytesLeft;

            IFileManagerService fmService = GetFileManagerService();

            try
            {
                sessionGuid = fmService.BeginChunkedFileDownload(out fileName, out fileSize,
                    fileGuid, false);
                fileStream = new FileStream(Path.Combine(targetDir, fileName),
                    FileMode.Create);
                fileBytesLeft = fileSize;

                while (fileBytesLeft > 0)
                {
                    chunkBytes = fmService.GetNextFileChunk(sessionGuid, chunkSize);
                    fileStream.Write(chunkBytes, 0, chunkBytes.Length);
                    fileBytesLeft -= chunkBytes.Length;
                }
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();

                if (sessionGuid != Guid.Empty)
                    fmService.EndChunkedFileDownload(sessionGuid);
            }
        }
        #endregion

        /// <summary>
        /// Creates and publishes a translation memory.
        /// </summary>
        /// <returns>The guid of the newly created TM.</returns>
        public static Guid CreateAndPublishTM(string tmName, string description, string sourceLanguageCode, string targetLanguageCode)
        {
            Utility.TMService.TMInfo tmInfo = new Utility.TMService.TMInfo()
            {
                Guid = Guid.NewGuid(), // This guid is ignored.
                Name = tmName,
                Description = description,
                Readonly = false,
                StoreFormatting = false,
                AllowMultiple = false,
                UseContext = false,
                SourceLanguageCode = sourceLanguageCode,
                TargetLanguageCode = targetLanguageCode
            };
            ITMService tmService = GetTMService();
            // CreateAndPublish returns the guid of the new TM
            return tmService.CreateAndPublish(tmInfo);
        }

        /// <summary>
        /// Creates and publishes a term base.
        /// </summary>
        /// <returns>The guid of the newly created TB.</returns>
        public static Guid CreateAndPublishTB(string tbName, string description, string[] languageCodes)
        {
            Utility.TBService.TBInfo tbInfo = new Utility.TBService.TBInfo()
            {
                Guid = Guid.NewGuid(), // This guid is ignored.
                Name = tbName,
                Description = description,
                Readonly = false
            };
            tbInfo.LanguageCodes = new string[languageCodes.Length];
            Array.Copy(languageCodes, tbInfo.LanguageCodes, languageCodes.Length);

            ITBService tbService = GetTBService();
            // CreateAndPublish returns the guid of the new TB
            return tbService.CreateAndPublish(tbInfo);
        }

        /// <summary>
        /// Creates a server project.
        /// </summary>
        /// <param name="name">The name of the project.</param>
        /// <param name="targetLanguageCodes">The target language codes of the
        /// project.</param>
        /// <returns>The guid of the newly created project.</returns>
        public static Guid CreateLiveDocsProject(string name, string sourceLanguageCode, string[] targetLanguageCodes, bool isVersioned,
            Guid userGuid, string subject, DateTime deadline, IServerProjectService serverProjectService)
        {
            TemplateBasedProjectCreateInfo tbpci = new TemplateBasedProjectCreateInfo()
            {
                Name = name,
                SourceLanguageCode = sourceLanguageCode,
                TargetLanguageCodes = targetLanguageCodes,
                Subject = subject,
                CreatorUser = userGuid,
            };

            ServerProjectDesktopDocsCreateInfo spCreateInfo = new ServerProjectDesktopDocsCreateInfo()
            {
                // Resulted in PathTooLongException, cut down a little bit
                Name = name,
                SourceLanguageCode = sourceLanguageCode,
                TargetLanguageCodes = targetLanguageCodes,
                Subject = subject,
                Deadline = deadline,
                CreatorUser = userGuid,
                RecordVersionHistory = isVersioned,
                EnableWebTrans = true
            };

            return serverProjectService.CreateProject(spCreateInfo);
        }

        public static Guid CreateTemplateBasedProject(string name, Guid templateGuid, string sourceLanguageCode, string[] targetLanguageCodes,
            Guid userGuid, string subject, DateTime deadline, IServerProjectService serverProjectService)
        {
            TemplateBasedProjectCreateInfo tbpci = new TemplateBasedProjectCreateInfo()
            {
                Project = name,
                Name = name,
                TemplateGuid = templateGuid,
                SourceLanguageCode = sourceLanguageCode,
                TargetLanguageCodes = targetLanguageCodes,
                Subject = subject,
                CreatorUser = userGuid,
            };

            Guid projectId = serverProjectService.CreateProjectFromTemplate(tbpci).ProjectGuid;

            if (deadline != null)
            {
                CLogger.WriteLog(ELogLevel.INFO, "Updating deadline of project: " + projectId + " to " + deadline);

                ServerProjectUpdateInfo spui = new ServerProjectUpdateInfo()
                {
                    ServerProjectGuid = projectId,
                    Project = name,
                    Subject = subject,
                    Deadline = deadline
                };

                serverProjectService.UpdateProject(spui);
            }
            return projectId;
        }
    }
}
