using System;

using System.Collections.Generic;

using System.Text;

using log4net;

using log4net.Config;

using System.Diagnostics;

using System.Reflection;



namespace ClayTablet.CT.Utility
{
 

        public static class CLogger
        {

            #region Members

            private static readonly ILog logger = LogManager.GetLogger(typeof(CLogger));



            #endregion



            #region Constructors

            static CLogger()
            {

                XmlConfigurator.Configure();

            }

            #endregion



            #region Methods



            public static void WriteLog(ELogLevel logLevel, String log)
            {

                if (logLevel.Equals(ELogLevel.DEBUG))
                {

                    logger.Debug(log);

                }

                else if (logLevel.Equals(ELogLevel.ERROR))
                {

                    logger.Error(log);

                }

                else if (logLevel.Equals(ELogLevel.FATAL))
                {

                    logger.Fatal(log);

                }

                else if (logLevel.Equals(ELogLevel.INFO))
                {

                    logger.Info(log);

                }

                else if (logLevel.Equals(ELogLevel.WARN))
                {

                    logger.Warn(log);

                }

            }

            public static void WriteLog(ELogLevel logLevel, String log, Exception e)
            {

                if (logLevel.Equals(ELogLevel.DEBUG))
                {

                    logger.Debug(log, e);

                }

                else if (logLevel.Equals(ELogLevel.ERROR))
                {

                    logger.Error(log, e);

                }

                else if (logLevel.Equals(ELogLevel.FATAL))
                {

                    logger.Fatal(log, e);

                }

                else if (logLevel.Equals(ELogLevel.INFO))
                {

                    logger.Info(log, e);

                }

                else if (logLevel.Equals(ELogLevel.WARN))
                {

                    logger.Warn(log, e);

                }

            }

            #endregion

        }

    

}
