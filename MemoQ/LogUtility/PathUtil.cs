﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ClayTablet.CT.Utility
{
    public class PathUtil
    {
        public static string getAppFolder()
        {

            string directoryName = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).DirectoryName;
            return directoryName;
        }


        public static string getFullPath4RelatedPath(string relatedPath)
        {
            string appFolder = getAppFolder();

            if (relatedPath == null)
                return null;

            if (Path.IsPathRooted(relatedPath) && (File.Exists(relatedPath) || Directory.Exists(relatedPath)))
                return relatedPath;

            if (relatedPath.StartsWith(@"\"))
                return appFolder + relatedPath;
            else
                return appFolder + @"\" + relatedPath;
        }
    }
}
