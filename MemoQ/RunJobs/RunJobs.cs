using System;
using ClayTablet.CT.Utility;
using ClayTablet.MemoQ;
using System.Configuration;
using System.Reflection;
using System.Diagnostics;

namespace ClayTablet.MemoQ.RunJobs
{
    public class RunJobs
    {
        public static void Main(string[] args)
        {
            String provider_Name = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.Name"].ToString();

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string assembly_version = fvi.ProductVersion;
            string assembly_name = fvi.ProductName;

            CLogger.WriteLog(ELogLevel.INFO, "Load Provider: [" + provider_Name + "(" + assembly_name + "-" + assembly_version + ") ] ...");

            ClayTablet.MemoQ.ReceiverJob receiverJober = new ClayTablet.MemoQ.ReceiverJob();
            ClayTablet.MemoQ.ProviderSenderJob senderJober = new ClayTablet.MemoQ.ProviderSenderJob();

            int pollingTMSTimeOut = 0;
             
            while (1 == 1)
            {
                int sleepTime = ReadAppSettingInterval();
                CLogger.WriteLog(ELogLevel.INFO, "Start to polling [" + assembly_name + " - " + assembly_version + "]");
                try
                {
                    CLogger.WriteLog(ELogLevel.INFO, "Running Provider Receiver");
                    receiverJober.Process(); 
                }
                catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Receiver error", e);
                }

                pollingTMSTimeOut -= sleepTime;
                try
                {
                    if (pollingTMSTimeOut <= 0)
                    {
                        pollingTMSTimeOut = ReadMemoQPollingInterval();
                        CLogger.WriteLog(ELogLevel.INFO, "Running Provider TMS Checking");
                        senderJober.Process();
                    }
                    else
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "Next polling of MemoQ server in " + pollingTMSTimeOut / 1000 + " seconds");
                    }
                }
                catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Sender error", e);
                }
                CLogger.WriteLog(ELogLevel.INFO, "Polling done, next polling in " + (sleepTime / 1000) + " seconds");
                System.Threading.Thread.Sleep(sleepTime);
            } 
        }

        protected static int ReadAppSettingInterval()
        {
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SleepTime"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SleepTime"], 10);
                }
            }
            catch (Exception exp)
            {
                CLogger.WriteLog(ELogLevel.WARN, "Cannot read configured sleep time, use 30 seconds as default", exp);
            }

            return 30000;
        }

        protected static int ReadMemoQPollingInterval()
        {
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MemoQ_Polling_Interval"] != null)
                {
                    return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MemoQ_Polling_Interval"], 10);
                }
            }
            catch (Exception exp)
            {
                CLogger.WriteLog(ELogLevel.WARN, "Cannot read configured MemoQ polling interval, use 360 seconds as default", exp);
            }

            return 360000;
        }
    }
}
