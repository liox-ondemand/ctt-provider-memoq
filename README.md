# Connector for MemoQ #


## License ##

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available in license.txt


## Testing standalone console application
Open MemoQ\MemoQ.sln in VisualStudio and build the solution. 

After the solution is built, all files required for to run the connector
as a standalone console application can be found under directory 
**dist\MemoQ_provider_standalone**.

Please edit MemoQ_Provider.exe.config to configure the connector, 
including various settings for connecting to MemoQ. 

Please contact Lionbridge support team at connectors@lionbridge.com to obtain
license key to access ClayTablet platform. The license key will be either: 

the legacy key - a pair of XML files (source.xml and target.xml), please place them 
under the "accounts" directory

or

the new key - a single JSON file and a "ClientID" string, please place the the JSON
file under the "accounts" directory, then edit MemoQ_Provider.exe.config, to configure
both the "ClayTablet.Provider.SourceAccount" and "ClayTablet.Provider.TargetAccount" 
settings to point to the JSON file, and configure the "ClayTablet.Provider.ClientId" 
setting to be the "ClientID" string

Now you can start the MemoQ connector by running MemoQ_Provider.exe from
under the **dist\MemoQ_provider_standalone** directory.

## Deploying Windows service
Open Provider_MemoQ_ALL\Provider_MemoQ_ALL.sln in VisualStudio and 
build the solution. 

After post build process, all files required for a fully deployable 
connector package can be found under directory **dist\MemoQ_Provider**.  

This built package is meant for deploying the connector as Windows service. 
Please edit MemoQ_Provider_Service.exe.config to configure the connector, 
including setting "ClayTablet.Provider.ServiceName" for Windows service name, 
and various settings for connecting to MemoQ. Once the configuration is 
done, run install.bat to install the connector as a Windows service. Please 
note that you will need administrator privilege to install Windows service.

Please contact Lionbridge support team at connectors@lionbridge.com to obtain
license key to access ClayTablet platform. The license key will be either: 

the legacy key - a pair of XML files (source.xml and target.xml), please place them 
under the "accounts" directory

or

the new key - a single JSON file and a "ClientID" string, please place the the JSON
file under the "accounts" directory, then edit MemoQ_Provider.exe.config, to configure
both the "ClayTablet.Provider.SourceAccount" and "ClayTablet.Provider.TargetAccount" 
settings to point to the JSON file, and configure the "ClayTablet.Provider.ClientId" 
setting to be the "ClientID" string

Now you can start the MemoQ connector by starting the Windows service.

You can run remove.bat to remove the Windows service.

## Limitations

The code currently does not handle job quotation case, so if user submits a job with
the option "send for quote" selected it will be ignored by the connector.

## Structure

Lib - project references  
LogUtility - logging library  
ProviderReceiver - Receive jobs  
ProviderSender - Sending jobs  
Provider_NTService - This schedules and runs MemoQ Provider as Windows service  
RunJobs - Run MemoQ Connector Jobs
Template - this is where delivery structure is kept  
 